import { connect } from 'react-redux'
import View from './view'
import * as UsersActions from '../../redux/users/actions'

const mapStateToProps = state => {
    return {
        error: state.users.isRegisterError,
        isFetching: state.users.isFetching
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        registerUser: (email, password) => {
            dispatch(UsersActions.registerUser(email, password))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)