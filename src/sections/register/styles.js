import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.main
    },
    form: {
        marginHorizontal: 16,
        marginTop: 16
    },
    registerView: {
        height: 64,
        justifyContent: 'center'
    },
    register: {
        backgroundColor: colors.white,
        borderColor: colors.white
    },
    registerLabel: {
        color: colors.darkMain
    }
})