import React from 'react'
import { View, SafeAreaView, Text, ActivityIndicator } from 'react-native'
import { Button, Input, Message } from '../../widgets'
import styles from './styles'
import * as colors from '../../commons/colors'

export default class Register extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: ''
        }
    }

    _register = () => {
        const { email, password } = this.state
        this.props.registerUser(email, password)
    }

    render() {
        const { email, password } = this.state
        const { error, isFetching} = this.props

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.form}>
                    <Input 
                        placeholder="E-mail"
                        icon="envelope"
                        value={email}
                        onChangeText={email => this.setState({ email })}
                    />
                    <Input 
                        placeholder="Contraseña"
                        icon="key"
                        value={password}
                        secureTextEntry={true}
                        onChangeText={password => this.setState({ password })}
                    />

                    {
                        isFetching &&
                        <View style={styles.registerView}>
                            <ActivityIndicator 
                                size="large" 
                                color={colors.white} 
                            />
                        </View>
                    }
                    {
                        !isFetching && 
                        <View style={styles.registerView}>
                            <Button 
                                label="Crear cuenta"
                                buttonStyle={styles.register} 
                                labelStyle={styles.registerLabel}
                                onPress={this._register} 
                            />
                        </View>
                    }

                    {
                        error &&
                        <Message 
                            error={error} 
                            message="Error al registrar el usuario" 
                        />
                    }
                </View>
            </SafeAreaView>
        )
    }
}

