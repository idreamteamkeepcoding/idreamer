import React from 'react'
import { View, SafeAreaView, FlatList, RefreshControl } from 'react-native'
import { Input, DreamCell, Category, Message, Button } from '../../widgets'
import styles from './styles'
import * as colors from '../../commons/colors'
import _ from 'lodash'
import { Actions } from 'react-native-router-flux'

export default class DreamSearch extends React.Component {
    static defaultProps = {
        query: '',
        category: 0,
        saved: false
    }

    constructor(props) {
        super(props)
        this.state = {
            query: props.query,
            category: props.category
        }
        this._initDreamsSearchDebounced = _.debounce((category, query) => {
            this.props.initDreamsSearch(category, query)
        }, 500)
        this.props.initDreamsSearch(props.category, props.query)
    }

    _search = () => {
        const { category, query } = this.state
        this.props.initDreamsSearch(category, query)
    }

    _onEndReached = ({ distanceFromEnd }) => {
        const { dreamsList, dreamsTotal, isFetching } = this.props
        const { query, category } = this.state
        if (
            distanceFromEnd > 100 &&
            !isFetching &&
            dreamsList.length &&
            dreamsList.length < dreamsTotal
        ) {
            this.props.updateDreamsSearchOffset(category, query)
        }
    }

    _keyExtractor = (item, index) => `${item.id}`

    _onItemTapped = item => {
        Actions.DreamDetail({ dream: item })
    }

    _renderItem = ({ item, index }) => (
        <DreamCell 
            item={item}
            onPress={() => this._onItemTapped(item)}
        />
    )

    _onCategoryTapped = category => {
        this.setState({ 
            category: category.id
        })
        this.props.initDreamsSearch(category.id, this.state.query)
    }

    _onQueryUpdated = query => {
        this.setState({
            query
        })
        this._initDreamsSearchDebounced(this.state.category, query)
    }

    _renderCategory = ({ item, index }) => (
        <Category 
            icon={item.icon}
            label={item.name}
            selected={item.id === this.state.category}
            onPress={() => this._onCategoryTapped(item)}
        />
    )

    _saveSearch = () => {
        const { query, category } = this.state
        this.props.postSearch({
            query,
            category
        })
    }

    _renderNoResultsText = () => {
        const { isFetching } = this.props
        if (isFetching) {
            return null
        }
        return (
            <View style={styles.noResults}>
                <Message
                    error={false}
                    message="No hay resultados"
                />
            </View>
        )
    }

    render() {
        const { query } = this.state
        const { dreamsList, isFetching } = this.props

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.form}>
                    <View style={styles.formContent}>
                        <Input 
                            placeholder="¿Qué sueño quieres hacer realidad?"
                            icon="search"
                            value={query}
                            onChangeText={query => this._onQueryUpdated(query)}
                        />
                        <FlatList
                            style={styles.categories}
                            data={global.categories}
                            keyExtractor={this._keyExtractor}
                            renderItem={this._renderCategory}
                            horizontal={true}
                            extraData={this.state}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                </View>
                <FlatList
                    data={dreamsList}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                    extraData={this.state}
                    ListEmptyComponent={this._renderNoResultsText}
                    contentContainerStyle={styles.listContent}
                    onEndReached={this._onEndReached}
                    onEndReachedThreshold={0.8}
                    refreshControl={
                        <RefreshControl
                          onRefresh={this._search}
                          refreshing={isFetching}
                          tintColor={colors.main}
                          colors={[colors.main]}
                        />
                    }
                />
                {
                    !this.props.saved && 
                    <View style={styles.bottom}>
                        <Button 
                            label="Guardar búsqueda"
                            icon="search-plus"
                            onPress={() => this._saveSearch()}
                        />
                    </View>
                }
            </SafeAreaView>
        )
    }
}