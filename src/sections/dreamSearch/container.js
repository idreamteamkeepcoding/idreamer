import { connect } from 'react-redux'
import View from './view'
import * as DreamsActions from '../../redux/dreams/actions'
import * as SearchesActions from '../../redux/searches/actions'

const mapStateToProps = state => {
    return {
        dreamsList: state.dreams.searchList,
        dreamsTotal: state.dreams.total,
        isFetching: state.dreams.isFetching
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        initDreamsSearch: (category, query) => {
            dispatch(DreamsActions.initDreamsSearch(category, query))
        },
        updateDreamsSearchOffset: (category, query) => {
            dispatch(DreamsActions.updateDreamsSearchOffset(category, query))
        },
        postSearch: (search) => {
            dispatch(SearchesActions.postSearch(search))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)