import { StyleSheet, Dimensions } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    form: {
        marginTop: 8,
        borderBottomWidth: 1,
        borderBottomColor: colors.border
    },
    formContent: {
        marginHorizontal: 16
    },
    categories: {
        marginBottom: 16
    },
    listContent: {
        paddingBottom: 80
    },
    noResults: {
        marginTop: 24,
        marginHorizontal: 16
    },
    bottom: {
        position: 'absolute',
        width: 240,
        bottom: 8,
        left: (Dimensions.get('window').width / 2) - 120,
        justifyContent: 'center'
    }
})