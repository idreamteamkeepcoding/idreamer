import React from 'react'
import { View, ScrollView, SafeAreaView, Text, TouchableOpacity } from 'react-native'
import { CachedImage } from 'react-native-cached-image'
import { Button, Category, IconNumber, User } from '../../widgets'
import MapView, { Marker } from 'react-native-maps'
import Icon from 'react-native-vector-icons/FontAwesome5'
import moment from 'moment'
import styles from './styles'
import * as colors from '../../commons/colors'
import * as dreamStates from '../../commons/dreamStates'
import { Actions } from 'react-native-router-flux'
import Share, { ShareSheet, Button as ShareButton } from 'react-native-share'
import _ from 'lodash'

const TWITTER_ICON = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAMAAAANIilAAAABvFBMVEUAAAAA//8AnuwAnOsAneoAm+oAm+oAm+oAm+oAm+kAnuwAmf8An+0AqtUAku0AnesAm+oAm+oAnesAqv8An+oAnuoAneoAnOkAmOoAm+oAm+oAn98AnOoAm+oAm+oAmuoAm+oAmekAnOsAm+sAmeYAnusAm+oAnOoAme0AnOoAnesAp+0Av/8Am+oAm+sAmuoAn+oAm+oAnOoAgP8Am+sAm+oAmuoAm+oAmusAmucAnOwAm+oAmusAm+oAm+oAm+kAmusAougAnOsAmukAn+wAm+sAnesAmeoAnekAmewAm+oAnOkAl+cAm+oAm+oAmukAn+sAmukAn+0Am+oAmOoAmesAm+oAm+oAm+kAme4AmesAm+oAjuMAmusAmuwAm+kAm+oAmuoAsesAm+0Am+oAneoAm+wAmusAm+oAm+oAm+gAnewAm+oAle0Am+oAm+oAmeYAmeoAmukAoOcAmuoAm+oAm+wAmuoAneoAnOkAgP8Am+oAm+oAn+8An+wAmusAnuwAs+YAmegAm+oAm+oAm+oAmuwAm+oAm+kAnesAmuoAmukAm+sAnukAnusAm+oAmuoAnOsAmukAqv9m+G5fAAAAlHRSTlMAAUSj3/v625IuNwVVBg6Z//J1Axhft5ol9ZEIrP7P8eIjZJcKdOU+RoO0HQTjtblK3VUCM/dg/a8rXesm9vSkTAtnaJ/gom5GKGNdINz4U1hRRdc+gPDm+R5L0wnQnUXzVg04uoVSW6HuIZGFHd7WFDxHK7P8eIbFsQRhrhBQtJAKN0prnKLvjBowjn8igenQfkQGdD8A7wAAAXRJREFUSMdjYBgFo2AUDCXAyMTMwsrGzsEJ5nBx41HKw4smwMfPKgAGgkLCIqJi4nj0SkhKoRotLSMAA7Jy8gIKing0KwkIKKsgC6gKIAM1dREN3Jo1gSq0tBF8HV1kvax6+moG+DULGBoZw/gmAqjA1Ay/s4HA3MISyrdC1WtthC9ebGwhquzsHRxBfCdUzc74Y9UFrtDVzd3D0wtVszd+zT6+KKr9UDX749UbEBgULIAbhODVHCoQFo5bb0QkXs1RAvhAtDFezTGx+DTHEchD8Ql4NCcSyoGJYTj1siQRzL/JKeY4NKcSzvxp6RmSWPVmZhHWnI3L1TlEFDu5edj15hcQU2gVqmHTa1pEXJFXXFKKqbmM2ALTuLC8Ak1vZRXRxa1xtS6q3ppaYrXG1NWjai1taCRCG6dJU3NLqy+ak10DGImx07LNFCOk2js6iXVyVzcLai7s6SWlbnIs6rOIbi8ViOifIDNx0uTRynoUjIIRAgALIFStaR5YjgAAAABJRU5ErkJggg=="
const FACEBOOK_ICON = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAMAAAANIilAAAAAYFBMVEUAAAAAQIAAWpwAX5kAX5gAX5gAX5gAXJwAXpgAWZ8AX5gAXaIAX5gAXpkAVaoAX5gAXJsAX5gAX5gAYJkAYJkAXpoAX5gAX5gAX5kAXpcAX5kAX5gAX5gAX5YAXpoAYJijtTrqAAAAIHRSTlMABFis4vv/JL0o4QvSegbnQPx8UHWwj4OUgo7Px061qCrcMv8AAAB0SURBVEjH7dK3DoAwDEVRqum9BwL//5dIscQEEjFiCPhubziTbVkc98dsx/V8UGnbIIQjXRvFQMZJCnScAR3nxQNcIqrqRqWHW8Qd6cY94oGER8STMVioZsQLLnEXw1mMr5OqFdGGS378wxgzZvwO5jiz2wFnjxABOufdfQAAAABJRU5ErkJggg=="
const EMAIL_ICON = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAMAAAANIilAAAABC1BMVEUAAAA/Pz8/Pz9AQEA/Pz8/Pz8+Pj4+Pj4/Pz8/Pz8/Pz8/Pz8+Pj4+Pj4/Pz8/Pz8/Pz9AQEA+Pj5AQEA/Pz87Ozs7Ozs/Pz8+Pj47OztAQEA/Pz89PT01NTVBQUFBQUE/Pz8/Pz8+Pj4/Pz9BQUE+Pj4/Pz8/Pz89PT0+Pj4/Pz9BQUFAQEA9PT09PT0/Pz87Ozs9PT05OTk/Pz8+Pj4/Pz9AQEA/Pz8/Pz8/Pz8/Pz+AgIA+Pj4/Pz8/Pz9AQEA/Pz8/Pz8/Pz8/Pz8+Pj4/Pz8/Pz8/Pz9AQEA+Pj4/Pz8+Pj4/Pz85OTk/Pz8/Pz8/Pz8/Pz88PDw9PT0/Pz88PDw8PDw+Pj45OTlktUJVAAAAWXRSTlMA/7N4w+lCWvSx8etGX/XlnmRO7+1KY/fjOGj44DU7UvndMec/VvLbLj7YKyiJdu9O7jZ6Um1w7DnzWQJz+tpE6uY9t8D9QehAOt7PVRt5q6duEVDwSEysSPRjqHMAAAEfSURBVEjH7ZTXUgIxGEa/TwURUFyKYgMURLCvbe2gYAV7ff8nMRksgEDiKl7lXOxM5p8zO3s2CWAwGAx/CjXontzT25Y+pezxtpv2+xTygJ+BYOvh4BBDwx1lKxxhNNZqNjLK+JjVWUYsykj4+2h8gpNTUMkIBuhPNE+SKU7PQC3D62E60ziYzXIuBx0Z+XRTc9F5fgF6MhKNzWXnRejKWGJdc9GZy8AP3kyurH52Ju01XTkjvnldNN+Qi03RecthfFtPlrXz8rmzi739Ax7mUCjy6FhH/vjPonmqVD6pdT718excLX/tsItLeRAqtc7VLIsFlVy/t6+ub27v7t8XD490niy3p+rZpv3i+jy/Or+5SUrdvcNcywaDwfD/vAF2TBl+G6XvQwAAAABJRU5ErkJggg=="

export default class DreamDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state ={
            isMine: props.dream.user === global.user.uid,
            share: false
        }
    }

    componentDidMount() {
        this.props.updateDreamData(this.props.dream)
        this.props.fetchUser(this.props.dream.user)
        this._updateIcons()
    }

    _updateIcons = () => {
        this.props.navigation.setParams({
            right: this._renderIcons
        })
    }

    _renderIcons = () => {
        const { dream } = this.props
        const favourited = dream.favourites && dream.favourites[global.user.uid]
        return (
            <View style={styles.notificationButtons}>
                {
                    this.state.isMine &&
                    <TouchableOpacity style={{ marginRight: 12 }} onPress={() => this._edit()}>
                        <Icon style={{ color: colors.white }} size={24} name="edit" solid={false} />            
                    </TouchableOpacity>
                }
                <TouchableOpacity style={{ marginRight: 12 }} onPress={() => this._share()}>
                    <Icon style={{ color: colors.white }} size={24} name="share" />            
                </TouchableOpacity>
                <TouchableOpacity style={{ marginRight: 12 }} onPress={() => this._changeFavourite()}>
                    <Icon style={{ color: favourited ? colors.red : colors.white }} size={24} name="heart" solid={favourited} />            
                </TouchableOpacity>                
            </View>
        )
    }

    _share = () => {
        this.setState({
            share: true
        })
    }

    _shareItem = (social) => {
        const { dream } = this.props
        this.setState({
            share: false
        })
        let shareOptions = {
            title: dream.name,
            message: `${dream.name}\n\n${dream.description}`,
            url: '',
            subject: 'Mira este sueño',
            social
        }
        Share.shareSingle(shareOptions)
    }

    _changeFavourite = () => {
        const { dream } = this.props
        const fav = dream.favourites ? dream.favourites : {}
        const favourited = fav[global.user.uid]

        if (favourited) {
            delete fav[global.user.uid] 
        } else {
            fav[global.user.uid] = true
        }

        this.props.favouriteDream(dream, fav, this._updateIcons)
        this.props.dream.favourites = fav
        this._updateIcons()
    }

    _help = () => {
        const { dream } = this.props
        const users = dream.users ? dream.users : {}
        const collab = users[global.user.uid]

        if (!collab) {
            users[global.user.uid] = true
        }

        this.props.helpDream(dream, users)
    }

    _onCategoryPressed = (category) => {
        Actions.DreamSearchOutside({
            category: category.id,
            saved: true,
            title: category.name
        })
    }

    _onUserPressed = () => {
        if (this.props.user.id === global.user.uid) {
            Actions.jump('UserProfile')
        } else {
            Actions.UserDetails({
                user: this.props.user
            })
        }
    }

    _chat = () => {
        Actions.DreamChat({ dream: this.props.dream })
    }

    _edit = () => {
        Actions.DreamEdit({ dream: this.props.dream })
    }

    render() {
        const { isMine, share } = this.state
        const { dream, isFetchingUser, user, userIsCollaborator } = this.props
        const hasUsers = dream.users && Object.keys(dream.users).length > 0
        const source = dream.image ? { uri: dream.image } : require('../../commons/images/default.png')
        const category = global.categories.find(c => c.id === dream.category)
        const favourites = dream.favourites ? Object.keys(dream.favourites).length : 0
        const collaborators = dream.users ? Object.keys(dream.users).length : 0

        let bg = colors.white
        let labelColor = colors.darkMain
        if (dream.state == dreamStates.STATE_CANCELED) {
            bg = colors.dreamCanceledBg
            labelColor = colors.dreamCanceled
        } else if (dream.state == dreamStates.STATE_COMPLETED) {
            bg = colors.dreamCompletedBg
            labelColor = colors.dreamCompleted
        }
        
        return (
            <SafeAreaView style={[styles.container, { backgroundColor: bg }]}>
                <CachedImage source={source} style={styles.image} />
                <View style={styles.content}>
                    <View style={styles.iconsView}>
                        <Category
                            label={category.name}
                            icon={category.icon}
                            selected={true}
                            style={styles.category}
                            onPress={() => this._onCategoryPressed(category)}
                        />
                        <View style={styles.icons}>
                            <IconNumber icon="heart" number={favourites} iconColor={colors.red} />
                            <IconNumber icon="handshake" number={collaborators} iconColor={colors.main} />
                        </View>
                    </View>
                    <Text style={[styles.title, { color: labelColor }]}>{dream.name}</Text>
                    <View style={styles.metadata}>
                        <Text style={styles.locationText}>{dream.location}</Text>
                        <Text style={styles.bull}>&bull;</Text>
                        <Text style={styles.dateText}>{moment(dream.date.toDate()).fromNow()}</Text>
                    </View>
                    <User user={user} loading={isFetchingUser} style={styles.user} onPress={() => this._onUserPressed()} />
                    <ScrollView style={styles.scroll}>
                        <Text style={styles.description}>{dream.description}</Text>
                        {
                            dream.coordinates &&
                            <MapView 
                                style={styles.map}
                                initialRegion={{
                                    latitude: dream.coordinates.latitude,
                                    longitude: dream.coordinates.longitude,
                                    latitudeDelta: 0.02,
                                    longitudeDelta: 0.01,
                                }}
                            >
                                <Marker
                                    coordinate={{
                                        latitude: dream.coordinates.latitude,
                                        longitude: dream.coordinates.longitude
                                    }}
                                    title={dream.name}
                                    description={dream.description}
                                />
                            </MapView>
                        }
                    </ScrollView>                    
                    <View style={styles.bottom}>
                    {
                        !userIsCollaborator && !isMine && dream.state === dreamStates.STATE_PUBLISHED &&
                        <Button 
                            label="Colaborar"
                            icon="handshake"
                            iconSolid={true}
                            onPress={() => this._help()} 
                        />
                    }
                    {
                        ((isMine && hasUsers) || userIsCollaborator) &&
                        <Button 
                            label="Chat"
                            icon="comment"
                            iconSolid={true}
                            onPress={() => this._chat()} 
                        />
                    }
                    </View>                 
                </View>
                <ShareSheet visible={share} onCancel={() => this.setState({ share: false })}>
                    <ShareButton 
                        iconSrc={{ uri: TWITTER_ICON }}
                        onPress={() => this._shareItem('twitter')}>
                        Twitter
                    </ShareButton>
                    <ShareButton 
                        iconSrc={{ uri: FACEBOOK_ICON }}
                        onPress={() => this._shareItem('facebook')}>
                        Facebook
                    </ShareButton>
                    <ShareButton 
                        iconSrc={{ uri: EMAIL_ICON }}
                        onPress={() => this._shareItem('email')}>
                        E-mail
                    </ShareButton>
                </ShareSheet>
            </SafeAreaView>
        )
    }
}


