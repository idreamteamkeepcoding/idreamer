import { StyleSheet, Dimensions } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1
    },
    notificationButtons: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }, 
    image: {
        width: '100%',
        height: 200,
        resizeMode: 'cover'
    },
    content: {
        flex: 1,
        padding: 16
    },
    iconsView: {
        flexDirection: 'row',
    },
    category: {
        alignSelf: 'flex-start'
    },
    icons: {
        flex: 1,
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    title: {
        fontSize: 18,
        fontFamily: 'Poppins-Bold',
        marginVertical: 6
    },
    metadata: {
        flexDirection: 'row',
        justifyContent: 'flex-start'
    }, 
    locationText: {
        fontSize: 14,
        fontFamily: 'Poppins-Bold',
        color: colors.grey,
        marginRight: 4
    },
    user: {
        marginVertical: 8
    }, 
    bull: {
        fontSize: 14,
        color: colors.lightGrey,
        marginRight: 4
    },
    dateText: {
        fontSize: 14,
        color: colors.grey
    },
    scroll: {
        paddingBottom: 90
    }, 
    description: {
        color: colors.black,
        marginTop: 8,
        marginBottom: 16
    },
    map: {
        width: '100%',
        height: 200
    },
    bottom: {
        position: 'absolute',
        width: 200,
        bottom: 8,
        left: (Dimensions.get('window').width / 2) - 100,
        justifyContent: 'center'
    }
})