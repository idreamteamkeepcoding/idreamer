import { connect } from 'react-redux'
import View from './view'
import * as DreamsActions from '../../redux/dreams/actions'
import * as UsersActions from '../../redux/users/actions'

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.users.user,
        isFetchingUser: state.users.isFetching,
        userIsCollaborator: state.dreams.userIsCollaborator
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchUser: (id) => {
            dispatch(UsersActions.fetchUser(id))
        },
        favouriteDream: (id, fav) => {
            dispatch(DreamsActions.favouriteDream(id, fav))
        },
        helpDream: (id, users) => {
            dispatch(DreamsActions.helpDream(id, users))
        },
        updateDreamData: (dream) => {
            dispatch(DreamsActions.updateDreamData(dream))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)