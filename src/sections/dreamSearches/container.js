import { connect } from 'react-redux'
import View from './view'
import * as SearchesActions from '../../redux/searches/actions'

const mapStateToProps = state => {
    return {
        searchesList: state.searches.list,
        isFetching: state.searches.isFetching
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchSearchesList: () => {
            dispatch(SearchesActions.fetchSearchesList())
        },
        deleteSearch: (id) => {
            dispatch(SearchesActions.deleteSearch(id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)