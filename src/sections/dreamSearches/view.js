import React from 'react'
import { SafeAreaView, FlatList, ActivityIndicator, View, Alert, RefreshControl } from 'react-native'
import { SearchCell, Message } from '../../widgets'
import styles from './styles'
import * as colors from '../../commons/colors'
import { Actions } from 'react-native-router-flux'

export default class DreamSearches extends React.Component {
    constructor(props) {
        super(props)
        this.props.fetchSearchesList()
    }

    _keyExtractor = (item, index) => `${item.id}`

    _onItemTapped = item => {
        Actions.DreamSearchOutside({ 
            query: item.query, 
            category: item.category, 
            saved: true 
        })
    }

    _renderItem = ({ item, index }) => (
        <SearchCell 
            categoryId={item.category}
            query={item.query}
            onPress={() => this._onItemTapped(item)}
            onLongPress={() => {
                Alert.alert(
                    '¡Atención!',
                    '¿Seguro que deseas borrar esta búsqueda?',
                    [
                        {
                            text: 'Cancelar',
                            style: 'cancel',
                        },
                        {
                            text: 'Aceptar', 
                            onPress: () => {
                                this.props.deleteSearch(item.id)
                            }
                        }
                    ],
                    {
                        cancelable: false
                    }
                )
            }}
        />
    )

    _renderFooter = (isFetching) => {
        if (isFetching) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator 
                        color={colors.main} 
                        size="large" 
                    />
                </View>
            )
        }
        return null 
    }

    _renderNoResultsText = () => {
        const { isFetching } = this.props
        if (isFetching) {
            return null
        }
        return (
            <View style={styles.noResults}>
                <Message
                    error={false}
                    message="No has guardado ninguna búsqueda"
                />
            </View>
        )
    }

    render() {
        const { searchesList, isFetching } = this.props
        return (
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={searchesList}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                    ListEmptyComponent={this._renderNoResultsText}
                    ListFooterComponent={_ => this._renderFooter(isFetching)}
                />
            </SafeAreaView>
        )
    }
}

