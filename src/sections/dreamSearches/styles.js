import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    loading: {
        height: 100,
        justifyContent: 'center'
    },
    noResults: {
        marginTop: 24,
        marginHorizontal: 16
    },
    toast: {
        backgroundColor: colors.main
    },
    toastText: {
        color: colors.white
    }
})