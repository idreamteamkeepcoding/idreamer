import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    modalContainer: {
        backgroundColor: colors.black,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center'
    },
    closeModal: {
        position: 'absolute',
        top: 40,
        right: 16    
    },
    container: {
        flex: 1
    },
    notificationButtons: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center'
    }, 
    noResults: {
        marginTop: 24,
        marginHorizontal: 16,
        transform: [{ rotateX: '180deg' }]
    },
    chat: {
        flex: 1
    }, 
    messages: {
        flex: 1
    },
    form: {
        flexDirection: 'row',
        padding: 10,
        height: 52
    },
    inputContainer: {
        flex: 1,
        marginRight: 10,
        marginVertical: 0
    },
    buttonImage: {
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        textAlign: 'center',
        backgroundColor: colors.darkMain,
        borderColor: colors.darkMain,
        padding: 0,
        paddingLeft: 8,
        height: 32,
        width: 32,
        borderRadius: 16,
        marginVertical: 0,
        marginRight: 10
    },
    button: {
        alignSelf: 'flex-start',
        paddingVertical: 6,
        paddingHorizontal: 10,
        height: 32,
        borderRadius: 16,
        marginVertical: 0
    },
    buttonLabel: {
        fontSize: 14
    }
})