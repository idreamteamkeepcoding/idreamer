import React from 'react'
import { View, SafeAreaView, FlatList, ActivityIndicator, Alert, TouchableOpacity, Text, Modal } from 'react-native'
import { Button, Input, Message, ChatMessage } from '../../widgets'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { CachedImage } from 'react-native-cached-image'
import ImagePicker from 'react-native-image-picker'
import ImageViewer from 'react-native-image-zoom-viewer'
import styles from './styles'
import * as colors from '../../commons/colors'

export default class DreamChat extends React.Component {
    constructor(props) {
        super(props)
        this.state ={
            isMine: props.dream.user === global.user.uid,
            message: '',
            isModalVisible: false,
            imageIndex: 0
        }
    }

    componentDidMount() {
        this.props.fetchUser()
        this.props.initChat()
        if (this.props.dream.user !== global.user.uid) {
            this.props.navigation.setParams({
                right: this._renderIcon
            })
        }
    }

    componentWillUnmount() {
        this.props.unsuscribeMessages()
    }

    _renderIcon = () => {
        return (
            <View style={styles.notificationButtons}>
                <TouchableOpacity style={{ marginRight: 12 }} onPress={() => this._confirmExit()}>
                    <Icon style={{ color: colors.white }} size={24} name="sign-out-alt" solid={false} />            
                </TouchableOpacity>                
            </View>
        )
    }

    _sendMessage = () => {
        const { message } = this.state
        if (message.trim() === '') {
            return
        }

        this.props.sendMessage(message)
        this.setState({
            message: ''
        })
    }

    _sendImage = () => {
        ImagePicker.launchImageLibrary(global.imagePickerOptions, (response) => {
            if (response.uri) {
                this.props.sendImage(response)    
            }
        })
    }

    _confirmExit = () => {
        Alert.alert(
            '¡Atención!',
            '¿Seguro que ya no deseas colaborar en este sueño?',
            [
                {
                    text: 'Cancelar',
                    style: 'cancel',
                },
                {
                    text: 'Aceptar', 
                    onPress: () => {
                        this._exit()
                    }
                }
            ],
            {
                cancelable: false
            }
        )
    }

    _exit = () => {
        const { dream } = this.props
        const users = dream.users ? dream.users : {}
        const collab = users[global.user.uid]

        if (collab) {
            delete users[global.user.uid]
        }

        this.props.helpDream(users)
    }

    _confirmKick = (id) => {
        if (this.props.dream.user === global.user.uid) {
            Alert.alert(
                '¡Atención!',
                '¿Seguro que ya no deseas que este usuario colabore en este sueño?',
                [
                    {
                        text: 'Cancelar',
                        style: 'cancel',
                    },
                    {
                        text: 'Aceptar', 
                        onPress: () => {
                            this._kick(id)
                        }
                    }
                ],
                {
                    cancelable: false
                }
            )
        }
    }

    _kick = (id) => {
        const { dream } = this.props
        const users = dream.users ? dream.users : {}
        const collab = users[id]

        if (collab) {
            delete users[id]
        }

        this.props.helpDream(users)
    }

    _keyExtractor = (item, index) => `${item.date}${item.user.id}`

    _renderItem = ({ item, index }) => (
        <ChatMessage 
            message={item} 
            onUserLongPress={() => this._confirmKick(item.user.id)}
            onImagePress={(url) => this._showModal(url)}
        />
    )

    _renderNoResultsText = () => {
        return (
            <View style={styles.noResults}>
                <Message
                    error={false}
                    message="No hay mensajes"
                />
            </View>
        )
    }

    _showModal = (url) => {
        // Find image to show first
        const { imageList } = this.props
        for (let i = 0; i < imageList.length; i++) {
            if (imageList[i].url === url) {
                this.setState({
                    imageIndex: i
                })
            }
        }
        this.setState({
            isModalVisible: true
        })
    }

    _hideModal = () => {
        this.setState({
            isModalVisible: false
        })
    }

    render() {
        const { isFetchingUser, isFetchingMessages, messageList, imageList } = this.props
        const { message, isModalVisible, imageIndex } = this.state

        if (isModalVisible) {
            return (
                <View style={styles.modalContainer}>
                    <Modal visible={true} transparent={false} onRequestClose={() => this._hideModal()}>
                        <ImageViewer 
                            imageUrls={imageList}
                            enablePreload={true}
                            enableSwipeDown={true}
                            index={imageIndex}
                            renderImage={(props) => <CachedImage {...props} />}
                            onCancel={() => this._hideModal()}
                        />
                        <TouchableOpacity style={styles.closeModal} onPress={() => this._hideModal()}>
                            <Icon name="times-circle" size={24} color={colors.white} />
                        </TouchableOpacity>
                    </Modal>
                </View>
            )
        }

        return (
            <SafeAreaView style={styles.container}>
                {
                    (isFetchingUser || isFetchingMessages) &&
                    <View style={styles.loading}>
                        <ActivityIndicator 
                            color={colors.main} 
                            size="large" 
                        />
                    </View>
                }
                {
                    !isFetchingUser && !isFetchingMessages &&
                    <View style={styles.chat}>
                        <FlatList 
                            style={styles.messages}
                            data={messageList}
                            keyExtractor={this._keyExtractor}
                            renderItem={this._renderItem}
                            extraData={this.state}
                            inverted={true}
                            ListEmptyComponent={this._renderNoResultsText}
                        />
                        <View style={styles.form}>
                            <Input
                                placeholder="Escribe un mensaje"
                                value={message}
                                containerStyle={styles.inputContainer}
                                onChangeText={message => {this.setState({ message })}}
                                onSubmitEditing={() => this._sendMessage()}
                            />
                            <Button
                                icon="file-image"
                                iconSize={17}
                                buttonStyle={styles.buttonImage}
                                onPress={() => this._sendImage()}
                            />
                            <Button
                                icon="paper-plane"
                                label="Enviar"
                                iconSize={18}
                                buttonStyle={styles.button}
                                labelStyle={styles.buttonLabel}
                                onPress={() => this._sendMessage()}
                            />
                        </View>
                    </View>
                }
            </SafeAreaView>
        )
    }
}


