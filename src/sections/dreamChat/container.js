import { connect } from 'react-redux'
import View from './view'
import * as ChatsActions from '../../redux/chats/actions'
import * as UsersActions from '../../redux/users/actions'
import * as DreamsActions from '../../redux/dreams/actions'

const mapStateToProps = (state, ownProps) => {
    return {
        messageList: state.chats.messageList,
        imageList: state.chats.imageList,
        user: state.users.currentUser,
        isFetchingUser: state.users.isFetching,
        isFetchingMessages: state.chats.isFetching
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchUser: () => {
            dispatch(UsersActions.fetchUser(global.user.uid))
        },
        helpDream: (users) => {
            dispatch(DreamsActions.helpDream(props.dream, users))
        },
        initChat: () => {
            dispatch(ChatsActions.initChat(props.dream))
        },
        unsuscribeMessages: () => {
            dispatch(ChatsActions.unsuscribeMessages())
        },
        sendMessage: (message) => {
            dispatch(ChatsActions.sendMessage(message, props.dream))
        },
        sendImage: (image) => {
            dispatch(ChatsActions.sendImage(image, props.dream))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)