import { connect } from 'react-redux'
import View from './view'
import * as UsersActions from '../../redux/users/actions'

const mapStateToProps = state => {
    return {
        user: state.users.currentUser,
        isFetching: state.users.isFetching
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchCurrentUser: () => {
            dispatch(UsersActions.fetchUser(global.user.uid))
        },
        logoutUser: () => {
            dispatch(UsersActions.logoutUser())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)