import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    content: {
        margin: 20
    },
    block: {
        marginBottom: 20
    },
    avatarView: {
        flexDirection: 'row'
    },
    avatar: {
        width: 32,
        height: 32,
        resizeMode: 'cover',
        borderRadius: 16,
        marginRight: 16,
        backgroundColor: colors.grey,
        alignItems: 'center',
        justifyContent: 'center'
    },
    userName: {
        lineHeight: 32,
        fontSize: 18,
        fontFamily: 'Poppins-Bold',
        color: colors.black
    },
    userNameLoading: {
        height: 32,
        width: 240,
        backgroundColor: colors.lightGrey,
        borderRadius: 16
    },
    bio: {
        fontSize: 14,
        color: colors.black
    },
    title: {
        marginTop: 40,
        backgroundColor: colors.main,
        padding: 8,
        borderTopWidth: 0.5,
        borderTopColor: colors.border,
        borderBottomWidth: 0.5,
        borderBottomColor: colors.border
    },
    titleText: {
        fontSize: 12,
        color: colors.white
    },
    exit: {
        backgroundColor: colors.white,
        borderColor: colors.white
    },
    exitLabel: {
        color: colors.main
    }
})