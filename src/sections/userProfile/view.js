import React from 'react'
import { View, ScrollView, SafeAreaView, Text, ActivityIndicator } from 'react-native'
import { Button } from '../../widgets'
import { CachedImage } from 'react-native-cached-image'
import Icon from 'react-native-vector-icons/FontAwesome5'
import styles from './styles'
import * as colors from '../../commons/colors'
import { Actions } from 'react-native-router-flux'
import _ from 'lodash'

export default class UserProfile extends React.Component {
    constructor(props) {
        super(props)
        this.props.fetchCurrentUser()
    }

    _editProfile = () => {
        Actions.UserEdit()
    }

    _logout = () => {
        this.props.logoutUser()
    }

    render() {
        const { user, isFetching } = this.props
        const userName = _.get(user, 'name', global.ANONYMOUS)
        const userAvatar = _.get(user, 'avatar', null)
        const userBio = _.get(user, 'bio', 'Cuéntanos algo sobre tí para que te conozcan el resto de soñadores.')
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.content}>
                    {
                        isFetching &&
                        <ActivityIndicator 
                            color={colors.main} 
                            size="large" 
                        />
                    }
                    {
                        !isFetching &&
                        <View>
                            <View style={[styles.block, styles.avatarView]}>
                                {
                                    userAvatar &&
                                    <CachedImage style={styles.avatar} source={{ uri: userAvatar }} />
                                } 
                                {
                                    !userAvatar &&
                                    <View style={styles.avatar}>
                                        <Icon name="user" color={colors.white} size={16} />
                                    </View>
                                }
                                <Text style={styles.userName}>{userName}</Text>
                            </View>
                            
                            <View style={styles.block}>
                                <Text style={styles.bio}>{userBio}</Text>
                            </View>
                            
                            <Button 
                                label="Editar perfil"
                                icon="edit"
                                onPress={() => this._editProfile()}
                            />
                        </View>
                    }                        
                    <Button 
                        label="Cerrar sesión"  
                        buttonStyle={styles.exit} 
                        labelStyle={styles.exitLabel}    
                        onPress={() => this._logout()}
                    />
                </ScrollView>
            </SafeAreaView>
        )
    }
}

