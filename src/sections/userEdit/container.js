import { connect } from 'react-redux'
import View from './view'
import * as UsersActions from '../../redux/users/actions'

const mapStateToProps = state => {
    return {
        user: state.users.currentUser,
        isFetching: state.users.isFetching
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        postUser: (user) => {
            dispatch(UsersActions.postUser(user))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)