import { StyleSheet, Dimensions } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    form: {
        marginHorizontal: 16,
        marginBottom: 64
    },
    avatarView: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        width: 128,
        height: 128,
        resizeMode: 'cover',
        borderRadius: 64,
        marginTop: 16
    },
    bottom: {
        position: 'absolute',
        width: 200,
        bottom: 24,
        left: (Dimensions.get('window').width / 2) - 100,
        justifyContent: 'center'
    },
    toast: {
        backgroundColor: colors.main
    },
    toastText: {
        color: colors.white
    }
})