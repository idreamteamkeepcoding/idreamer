import React from 'react'
import { View, Image, ScrollView, SafeAreaView, ActivityIndicator } from 'react-native'
import { Input, Button } from '../../widgets'
import ImagePicker from 'react-native-image-picker'
import styles from './styles'
import * as colors from '../../commons/colors'
import _ from 'lodash'

export default class UserProfile extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: _.get(props, 'user.name', ''),
            avatar: props.user && props.user.avatar ? { uri: props.user.avatar } : null,
            bio: _.get(props, 'user.bio', null)
        }
    }

    _addAvatar = () => {
        ImagePicker.launchImageLibrary(global.imagePickerOptions, (response) => {
            if (response.uri) {
                this.setState({ 
                    avatar: response          
                })
            }
        })
    }

    _removeAvatar = () => {
        this.setState({
            avatar: null
        })
    }

    _updateProfile = () => {
        this.props.postUser(this.state)
    }

    render() {
        const { name, avatar, bio } = this.state
        const { isFetching } = this.props
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.form}>
                    {
                        avatar &&
                        <View style={styles.avatarView}>
                            <Image
                                source={{ uri: avatar.uri }}
                                style={styles.avatar}
                            />
                            <Button 
                                label="Quitar avatar"
                                icon="minus"
                                buttonStyle={{ flex: 1, width: '100%' }}
                                onPress={() => this._removeAvatar()}
                            />
                        </View>
                    }
                    {
                        !avatar &&
                        <Button 
                            label="Añadir avatar"
                            icon="plus"
                            onPress={() => this._addAvatar()}
                        />
                    } 
                    <Input
                        label="Nombre"
                        placeholder="¿Con qué nombre quieres que te conozcan?"
                        icon="user"
                        value={name}
                        onChangeText={name => this.setState({ name })}
                    />
                    <Input
                        label="Bio"
                        placeholder="Háblanos sobre tí para que los demás soñadores te conozcan mejor..."
                        icon="font"
                        value={bio}
                        onChangeText={bio => this.setState({ bio })}
                        multiline={true}
                        inputStyle={{ height: 200 }}
                    />
                </ScrollView>
                <View style={styles.bottom}>
                    {
                        !isFetching &&
                        <Button 
                            label="Actualizar perfil"
                            icon="check"
                            onPress={() => this._updateProfile()}
                        />
                    }
                    {
                        isFetching &&
                        <ActivityIndicator 
                            color={colors.main} 
                            size="large" 
                        />
                    }
                </View>
            </SafeAreaView>
        )
    }
}

