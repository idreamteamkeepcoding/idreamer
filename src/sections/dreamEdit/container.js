import { connect } from 'react-redux'
import { DreamForm } from '../../widgets'
import * as DreamsActions from '../../redux/dreams/actions'

const mapStateToProps = state => {
    return {
        saving: state.dreams.isFetching
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onSubmit: (dream, photo) => {
            dispatch(DreamsActions.updateDream(dream, photo))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DreamForm)