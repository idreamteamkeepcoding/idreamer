import { connect } from 'react-redux'
import View from './view'
import * as UsersActions from '../../redux/users/actions'

const mapStateToProps = state => {
    return {
        isFetching: state.users.isFetching,
        success: state.users.isRememberSuccess,
        error: state.users.isRememberError
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        rememberPassword: (email) => {
            dispatch(UsersActions.rememberPassword(email))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)