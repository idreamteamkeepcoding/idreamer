import React from 'react'
import { View, SafeAreaView, Text, ActivityIndicator } from 'react-native'
import { Button, Input, Message } from '../../widgets'
import styles from './styles'
import * as colors from '../../commons/colors'

export default class RememberPassword extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email: ''
        }
    }

    _remember = () => {
        this.props.rememberPassword(this.state.email)
    }

    render() {
        const { email } = this.state
        const { error, success, isFetching } = this.props

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.form}>
                    <Input 
                        placeholder="E-mail"
                        icon="envelope"
                        value={email}
                        onChangeText={email => this.setState({ email })}
                    />

                    {
                        isFetching  &&
                        <View style={styles.buttonView}>
                            <ActivityIndicator 
                                size="large" 
                                color={colors.white} 
                            />
                        </View>
                    }
                    {
                        !isFetching && 
                        <View style={styles.buttonView}>
                            <Button 
                                label="Comprobar e-mail"
                                buttonStyle={styles.remember} 
                                labelStyle={styles.rememberLabel}
                                onPress={this._remember} 
                            />
                        </View>
                    }
                    {
                        error &&
                        <Message
                            error={true}
                            message="No se encontró al usuario"
                        />
                    }
                    {
                        success &&
                        <Message
                            error={false}
                            message="Revisa tu correo y sigue las instrucciones para cambiar tu contraseña"
                        />
                    }
                </View>
            </SafeAreaView>
        )
    }
}

