import { connect } from 'react-redux'
import View from './view'
import * as DreamsActions from '../../redux/dreams/actions'

const mapStateToProps = state => {
    return {
        userList: state.dreams.ownUserList,
        favouriteList: state.dreams.ownFavouriteList,
        sharedList: state.dreams.ownSharedList,
        isFetching: state.dreams.isFetching
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchUserDreams: () => {
            dispatch(DreamsActions.fetchUserDreams(global.user.uid))
        },
        fetchFavouriteDreams: () => {
            dispatch(DreamsActions.fetchFavouriteDreams(global.user.uid))
        },
        fetchSharedDreams: () => {
            dispatch(DreamsActions.fetchSharedDreams(global.user.uid))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)