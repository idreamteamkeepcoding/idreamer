import { StyleSheet, Dimensions } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    header: {
        borderBottomWidth: 1,
        borderColor: colors.border
    }, 
    menuWrapper: {
        margin: 16,
    },
    menu: {
        color: colors.main,
        backgroundColor: colors.lightWhite
    },
    loading: {
        height: 100,
        justifyContent: 'center'
    },
    listContent: {
        paddingBottom: 80
    },
    noResults: {
        marginTop: 24,
        marginHorizontal: 16
    },
    bottom: {
        position: 'absolute',
        width: 200,
        bottom: 8,
        left: (Dimensions.get('window').width / 2) - 100,
        justifyContent: 'center'
    }
})