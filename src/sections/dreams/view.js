import React from 'react'
import { View, SafeAreaView, FlatList, RefreshControl } from 'react-native'
import { DreamCell, Message, Button } from '../../widgets'
import SegmentedControlTab from 'react-native-segmented-control-tab'
import styles from './styles'
import * as colors from '../../commons/colors'
import { Actions } from 'react-native-router-flux'

export default class DreamSearches extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            index: 0
        }
        this._search()
    }

    _search = () => {
        switch(this.state.index) {
            case 0:
                this.props.fetchUserDreams()
                break
            
            case 1:
                this.props.fetchFavouriteDreams()
                break

            case 2:
                this.props.fetchSharedDreams()
                break
        }
    }

    _keyExtractor = (item, index) => `${item.id}`

    _onItemTapped = item => {
        Actions.DreamDetail({ dream: item })
    }

    _renderItem = ({ item, index }) => (
        <DreamCell 
            item={item}
            onPress={() => this._onItemTapped(item)}
        />
    )

    _changeMenu = (index) => {
        this.setState({
            index
        })
        switch(index) {
            case 0:
                this.props.fetchUserDreams()
                break
            
            case 1:
                this.props.fetchFavouriteDreams()
                break

            case 2:
                this.props.fetchSharedDreams()
                break
        }
    }

    _renderNoResultsText = () => {
        const { isFetching } = this.props
        if (isFetching) {
            return null
        }
        return (
            <View style={styles.noResults}>
                <Message
                    error={false}
                    message="No hay resultados"
                />
            </View>
        )
    }

    _createDream = () => {
        Actions.DreamCreate()
    }

    render() {
        const { index } = this.state
        const { userList, favouriteList, sharedList, isFetching } = this.props
        let list
        switch(index) {
            case 0:
                list = userList
                break

            case 1:
                list = favouriteList
                break

            case 2:
                list = sharedList
                break
        }
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <SegmentedControlTab
                        tabsContainerStyle={styles.menuWrapper}
                        tabStyle={styles.menu}
                        values={['Míos', 'Favoritos', 'Participados']}
                        selectedIndex={index}
                        onTabPress={this._changeMenu}
                    />
                </View>
                <FlatList
                    data={list}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                    extraData={this.state}
                    contentContainerStyle={index === 0 ? styles.listContent : null}
                    ListEmptyComponent={this._renderNoResultsText}
                    refreshControl={
                        <RefreshControl
                          onRefresh={this._search}
                          refreshing={isFetching}
                          tintColor={colors.main}
                          colors={[colors.main]}
                        />
                    }
                />
                {
                    index === 0 &&
                    <View style={styles.bottom}>
                        <Button
                            label="Nuevo sueño"
                            icon="plus"
                            onPress={() => this._createDream()}
                        />
                    </View>
                }
            </SafeAreaView>
        )
    }
}

