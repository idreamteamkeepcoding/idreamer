import React from 'react'
import { View, ScrollView, SafeAreaView, FlatList, RefreshControl, Text } from 'react-native'
import { DreamCell, Message, Button } from '../../widgets'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { CachedImage } from 'react-native-cached-image'
import SegmentedControlTab from 'react-native-segmented-control-tab'
import styles from './styles'
import * as colors from '../../commons/colors'
import { Actions } from 'react-native-router-flux'
import _ from 'lodash'

export default class UserDetails extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            index: 0
        }
        props.fetchUserDreams()
    }

    _search = () => {
        switch(this.state.index) {
            case 0:
                this.props.fetchUserDreams()
                break
            
            case 1:
                this.props.fetchFavouriteDreams()
                break

            case 2:
                this.props.fetchSharedDreams()
                break
        }
    }

    _keyExtractor = (item, index) => `${item.id}`

    _onItemTapped = item => {
        Actions.DreamDetail({ dream: item })
    }

    _renderItem = ({ item, index }) => (
        <DreamCell 
            item={item}
            onPress={() => this._onItemTapped(item)}
        />
    )

    _changeMenu = (index) => {
        this.setState({
            index
        })
        switch(index) {
            case 0:
                this.props.fetchUserDreams()
                break
            
            case 1:
                this.props.fetchFavouriteDreams()
                break

            case 2:
                this.props.fetchSharedDreams()
                break
        }
    }

    _renderNoResultsText = () => {
        const { isFetching } = this.props
        if (isFetching) {
            return null
        }
        return (
            <View style={styles.noResults}>
                <Message
                    error={false}
                    message="No hay resultados"
                />
            </View>
        )
    }

    _createDream = () => {
        Actions.DreamCreate()
    }

    render() {
        const { index } = this.state
        const { user, userList, favouriteList, sharedList, isFetching } = this.props
        const userName = _.get(user, 'name', global.ANONYMOUS)
        const userAvatar = _.get(user, 'avatar', null)
        const userBio = _.get(user, 'bio', '')

        let list
        switch(index) {
            case 0:
                list = userList
                break

            case 1:
                list = favouriteList
                break

            case 2:
                list = sharedList
                break
        }

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.userDetails}>
                    <View style={styles.avatarView}>
                        {
                            userAvatar &&
                            <CachedImage style={styles.avatar} source={{ uri: userAvatar }} />
                        } 
                        {
                            !userAvatar &&
                            <View style={styles.avatar}>
                                <Icon name="user" color={colors.white} size={16} />
                            </View>
                        }
                        <Text style={styles.userName}>{userName}</Text>
                    </View>
                    
                    <ScrollView style={styles.bioView}>
                        <Text style={styles.bio}>{userBio}</Text>
                    </ScrollView>
                </View>

                <View style={styles.header}>
                    <SegmentedControlTab
                        tabsContainerStyle={styles.menuWrapper}
                        tabStyle={styles.menu}
                        values={['Creados', 'Favoritos', 'Participados']}
                        selectedIndex={index}
                        onTabPress={this._changeMenu}
                    />
                </View>
                <FlatList
                    data={list}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                    extraData={this.state}
                    contentContainerStyle={styles.listContent}
                    ListEmptyComponent={this._renderNoResultsText}
                    refreshControl={
                        <RefreshControl
                          onRefresh={this._search}
                          refreshing={isFetching}
                          tintColor={colors.main}
                          colors={[colors.main]}
                        />
                    }
                />
            </SafeAreaView>
        )
    }
}

