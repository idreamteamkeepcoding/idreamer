import { connect } from 'react-redux'
import View from './view'
import * as DreamsActions from '../../redux/dreams/actions'

const mapStateToProps = state => {
    return {
        userList: state.dreams.userList,
        favouriteList: state.dreams.favouriteList,
        sharedList: state.dreams.sharedList,
        isFetching: state.dreams.isFetching
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchUserDreams: () => {
            dispatch(DreamsActions.fetchUserDreams(props.user.id))
        },
        fetchFavouriteDreams: () => {
            dispatch(DreamsActions.fetchFavouriteDreams(props.user.id))
        },
        fetchSharedDreams: () => {
            dispatch(DreamsActions.fetchSharedDreams(props.user.id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)