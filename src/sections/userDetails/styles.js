import { StyleSheet, Dimensions } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    userDetails: {
        padding: 16,
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
        backgroundColor: colors.lightWhite
    },
    avatarView: {
        flexDirection: 'row',
        marginBottom: 20
    },
    avatar: {
        width: 32,
        height: 32,
        resizeMode: 'cover',
        borderRadius: 16,
        marginRight: 16,
        backgroundColor: colors.grey,
        alignItems: 'center',
        justifyContent: 'center'
    },
    userName: {
        lineHeight: 32,
        fontSize: 18,
        fontFamily: 'Poppins-Bold',
        color: colors.black
    },
    userNameLoading: {
        height: 32,
        width: 240,
        backgroundColor: colors.lightGrey,
        borderRadius: 16
    },
    bioView: {
        maxHeight: 80
    },
    bio: {
        fontSize: 14,
        color: colors.black,
    },
    header: {
        borderBottomWidth: 1,
        borderColor: colors.border
    }, 
    menuWrapper: {
        margin: 16,
    },
    menu: {
        color: colors.main,
        backgroundColor: colors.lightWhite
    },
    listContent: {
        paddingBottom: 80
    },
    noResults: {
        marginTop: 24,
        marginHorizontal: 16
    },
    bottom: {
        position: 'absolute',
        width: 200,
        bottom: 8,
        left: (Dimensions.get('window').width / 2) - 100,
        justifyContent: 'center'
    }
})