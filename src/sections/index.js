import Login from './login'
import Register from './register'
import RememberPassword from './rememberPassword'

import DreamCreate from './dreamCreate'
import DreamDetail from './dreamDetail'
import DreamChat from './dreamChat'
import DreamEdit from './dreamEdit'
import DreamSearch from './dreamSearch'
import DreamSearches from './dreamSearches'
import Dreams from './dreams'

import UserProfile from './userProfile'
import UserDetails from './userDetails'
import UserEdit from './userEdit'

export {
    Login,
    Register,
    RememberPassword,

    DreamCreate,
    DreamDetail,
    DreamChat,
    DreamEdit,
    DreamSearch,
    DreamSearches,
    Dreams,

    UserProfile,
    UserDetails,
    UserEdit
}