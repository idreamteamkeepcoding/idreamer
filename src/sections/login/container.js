import { connect } from 'react-redux'
import View from './view'
import * as UsersActions from '../../redux/users/actions'

const mapStateToProps = state => {
    return {
        error: state.users.isLoginError,
        isFetching: state.users.isFetching
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        loginUser: (email, password) => {
            dispatch(UsersActions.loginUser(email, password))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(View)