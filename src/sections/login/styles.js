import { StyleSheet, Platform } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.darkMain
    },
    safeContainer: {
        flex: 1
    },
    bg: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        opacity: 0.4
    }, 
    form: {
        marginHorizontal: 16
    },
    logo: {
        maxWidth: '90%',
        resizeMode: 'contain',
        justifyContent: 'center',
        alignSelf: 'center',
        ...Platform.select({
            android: {
                marginTop: 32
            },
        })
    },
    mark: {
        textAlign: 'center',
        color: colors.white,
        fontSize: 24,
        fontFamily: 'Poppins-Bold',
        marginVertical: 64
    },
    loginView: {
        height: 64,
        justifyContent: 'center'
    },
    login: {
        backgroundColor: colors.white,
        borderColor: colors.white
    },
    loginLabel: {
        color: colors.darkMain
    },
    forgot: {
        backgroundColor: 'transparent',
        borderColor: 'transparent'
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 8,
        marginHorizontal: 16
    },
    register: {
        backgroundColor: colors.lightMain,
        borderColor: colors.lightMain
    },
    registerLabel: {
        color: colors.darkMain
    }
})