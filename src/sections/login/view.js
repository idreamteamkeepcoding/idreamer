import React from 'react'
import { View, SafeAreaView, Text, ActivityIndicator, Image } from 'react-native'
import { Button, Input, Message } from '../../widgets'
import styles from './styles'
import * as colors from '../../commons/colors'
import { Actions } from 'react-native-router-flux'

export default class Login extends React.Component {
    constructor(props) {
        super(props)
        this.subscriber = null
        this.state = {
            message: '',
            email: 'prueba@idreamer.com',
            password: 'Prueba123'
        }
    }

    componentWillUnmount() {
        if (this.unsubscriber) {
            this.unsubscriber()
        }
    }

    _login = () => {
        const { email, password } = this.state
        this.props.loginUser(email, password)
    }
    
    render() {
        const { email, password } = this.state
        const { isFetching, error } = this.props
        return (
            <View style={styles.container}>
                <Image style={styles.bg} source={require('../../commons/images/bg.jpg')} />
                <SafeAreaView style={styles.safeContainer}>                    
                    <Image style={styles.logo} source={require('../../commons/images/iDreamer.png')} />
                    <Text style={styles.mark}>Soñar es gratis</Text>
                    <View style={styles.form}>
                        <Input 
                            placeholder="E-mail"
                            icon="envelope"
                            value={email}
                            onChangeText={email => this.setState({ email })}
                        />
                        <Input 
                            placeholder="Contraseña"
                            icon="key"
                            value={password}
                            secureTextEntry={true}
                            onChangeText={password => this.setState({ password })}
                        />

                        {
                            isFetching &&
                            <View style={styles.loginView}>
                                <ActivityIndicator 
                                    size="large" 
                                    color={colors.white} 
                                />
                            </View>
                        }
                        {
                            !isFetching && 
                            <View style={styles.loginView}>
                                <Button 
                                    label="Entrar"
                                    buttonStyle={styles.login} 
                                    labelStyle={styles.loginLabel}
                                    onPress={this._login} 
                                />
                            </View>
                        }
                        
                        <Button 
                            label="He olvidado mi contraseña"
                            buttonStyle={styles.forgot} 
                            labelStyle={styles.forgotLabel}
                            onPress={() => Actions.RememberPassword()} 
                        />

                        {
                            error &&
                            <Message 
                                error={error} 
                                message="Credenciales inválidos" 
                            />
                        }
                    </View>
                    <View style={styles.bottom}>
                        <Button 
                            label="Registrarse"
                            buttonStyle={styles.register} 
                            labelStyle={styles.registerLabel}
                            onPress={() => Actions.Register()} 
                        />
                    </View>
                </SafeAreaView>
            </View>
        )
    }
}

