import React from 'react'
import { View, StyleSheet, PixelRatio, Text, StatusBar, Platform } from 'react-native'
import { Stack, Router, Scene, Tabs } from 'react-native-router-flux'
import firebase from 'react-native-firebase'
import Icon from 'react-native-vector-icons/FontAwesome5'
import * as colors from './commons/colors'
import { configureGlobals } from './commons/global'
import { store } from './config/redux'
import { Provider } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import { 
    Login, 
    Register, 
    RememberPassword, 
    
    DreamCreate, 
    DreamEdit, 
    DreamDetail, 
    DreamChat,
    DreamSearch,
    DreamSearches, 
    Dreams, 
    
    UserProfile,
    UserDetails,
    UserEdit
} from './sections'

class TabIcon extends React.Component {
    render() {
        var color = this.props.focused ? colors.main : colors.border

        return (
            <View style={{flex:1, flexDirection:'column', alignItems:'center', alignSelf:'center', justifyContent: 'center'}}>
                <Icon style={{color: color}} name={this.props.iconName || "circle"} size={20} solid />
            </View>
        )
    }
}

export default class App extends React.Component {
    constructor(props) {
        super(props)
        configureGlobals()
        this.unsubscriber = null
        this.state = {
            user: null
        }

        // Change status bar style
        StatusBar.setBarStyle('light-content', true)
        if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor('rgba(0, 0, 0, 0)')
            StatusBar.setTranslucent(true)
        } 
            
        // Change default font
        let oldRender = Text.render
        Text.render = function (...args) {
            let origin = oldRender.call(this, ...args)
            return React.cloneElement(origin, {
                style: [{fontFamily: 'Poppins-Regular'}, origin.props.style]
            })
        }
    }

    componentDidMount() {
        this.unsubscriber = firebase.auth().onAuthStateChanged((user) => {
            this.setState({ user })
            global.user = user

            if (user) {
                // Insert device token
                firebase.messaging().getToken()
                    .then(deviceToken => {
                        firebase.firestore().collection('users').doc(global.user.uid)
                            .get()
                            .then((d) => {
                                const data = d.data()
                                let deviceTokens = {}
                                if (data && data.deviceTokens) {
                                    deviceTokens = data.deviceTokens
                                } 
                                deviceTokens[deviceToken] = true
                                
                                if (data) {
                                    firebase.firestore().collection('users').doc(global.user.uid)
                                        .update({
                                            deviceTokens
                                        })
                                    .catch((error) => {
                                        console.error('insertDeviceToken err:', error)
                                    })
                                } else {
                                    firebase.firestore().collection('users').doc(global.user.uid)
                                        .set({
                                            deviceTokens
                                        })
                                    .catch((error) => {
                                        console.error('insertDeviceToken err:', error)
                                    })
                                }
                            })
                            .catch((error) => {
                                console.error('insertDeviceToken err:', error)
                            })
                    })
                    .catch((error) => {
                        console.error('insertDeviceToken err:', error)
                    })
            }

            // Notification permissions
            firebase.messaging().hasPermission()
                .then(enabled => {
                    if (!enabled) {
                        firebase.messaging().requestPermission()
                            .then(() => {
                                console.log('Notifications enabled')
                            })
                            .catch(() => {
                                console.log('Error enabling notifications')
                            })
                    }
                })

            // Listening for notifications
            this.removeNotificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
                console.log('Notification displayed')
            })
            this.removeNotificationListener = firebase.notifications().onNotification((notification) => {
                console.log('Notification received')
            })

        })
    }

    componentWillUnmount() {
        if (this.unsubscriber) {
            this.unsubscriber()
            this.removeNotificationDisplayedListener()
            this.removeNotificationListener()
        }
    }

    render() {
        if (!this.state.user) {
            return (
                <Provider store={store}>
                    <Router>
                        <Stack key="root">
                            <Scene 
                                key="Login"
                                component={Login} 
                                title="Acceso"
                                hideNavBar
                                initial 
                            />
                            <Scene 
                                key="Register" 
                                component={Register}
                                {...navBarStyles} 
                                title="Registro" 
                            />
                            <Scene 
                                key="RememberPassword"
                                component={RememberPassword}
                                {...navBarStyles} 
                                title="Recordar contraseña" 
                            />
                        </Stack>
                    </Router>
                </Provider>   
            )
        }

        return (
            <Provider store={store}>
                <Router>
                    <Stack key="root">
                        <Tabs
                            tabs
                            tabBarStyle={styles.tabBar}
                            default="Home"
                            hideNavBar
                            activeTintColor={colors.main}
                            >
                            <Scene 
                                key="DreamSearch" 
                                component={DreamSearch}
                                iconName="home"
                                icon={TabIcon}
                                {...navBarStyles}
                                title="Inicio"
                                onEnter={() => Actions.refresh({ key: Math.random() })}
                                initial
                            />
                            <Scene 
                                key="DreamSearches" 
                                component={DreamSearches}
                                iconName="search"
                                icon={TabIcon}
                                {...navBarStyles}
                                title="Búsquedas"
                            />
                            <Scene 
                                key="Dreams" 
                                component={Dreams}
                                iconName="star"
                                icon={TabIcon}
                                {...navBarStyles}
                                title="Sueños"
                            />
                            <Scene 
                                key="UserProfile"
                                component={UserProfile}
                                iconName="user"
                                icon={TabIcon}
                                {...navBarStyles}
                                title="Perfil" 
                            />
                        </Tabs>

                        <Scene 
                            key="DreamCreate"
                            component={DreamCreate}
                            {...navBarStyles}
                            title="Nuevo sueño" 
                        />
                        <Scene 
                            key="DreamEdit"
                            component={DreamEdit}
                            {...navBarStyles}
                            title="Editar sueño" 
                        />
                        <Scene 
                            key="DreamDetail"
                            component={DreamDetail}
                            {...navBarStyles}
                            title="Sueño" 
                        />
                        <Scene 
                            key="DreamChat" 
                            component={DreamChat}
                            title="Chat"
                            {...navBarStyles}
                        />
                        <Scene 
                            key="DreamSearchOutside" 
                            component={DreamSearch}
                            title="Búsqueda guardada"
                            {...navBarStyles}
                        />

                        <Scene 
                            key="UserDetails" 
                            component={UserDetails}
                            title="Perfil usuario"
                            {...navBarStyles}
                        />
                        <Scene 
                            key="UserEdit" 
                            component={UserEdit}
                            title="Editar perfil"
                            {...navBarStyles}
                        />
                    </Stack>
                </Router>
            </Provider>
        )
    }
}

const navBarStyles = {
    navigationBarStyle: {
        backgroundColor: colors.navBar,
        ...Platform.select({
            android: {
                paddingTop: StatusBar.currentHeight,
                height: 74
            }
        })
    },
    titleStyle: {
        color: colors.white
    },
    backButtonTextStyle: {
        color: colors.white
    },
    backButtonTintColor: colors.white
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    tabBar: {
      borderTopColor: colors.border,
      borderTopWidth: 1 / PixelRatio.get(),
      backgroundColor: 'ghostwhite',
      opacity: 0.98
    }
})