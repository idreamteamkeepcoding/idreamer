import chats from './chats/reducer'
import dreams from './dreams/reducer'
import searches from './searches/reducer'
import users from './users/reducer'

export {
    chats,
    dreams,
    searches,
    users
}