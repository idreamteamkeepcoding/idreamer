import * as types from './types'
import { Alert } from 'react-native'
import firebase from 'react-native-firebase'
import { Actions } from 'react-native-router-flux'
import { uploadImage } from '../../commons/global'
import _ from 'lodash'

const usersRef = firebase.firestore().collection('users')

function updateUser(user) {
    return {
        type: types.USERS_UPDATE_USER,
        user
    }
}

function updateCurrentUser(user) {
    return {
        type: types.USERS_UPDATE_CURRENTUSER,
        user
    }
}

function updateFetching(value) {
    return {
        type: types.USERS_UPDATE_FETCHING,
        value
    }
}

function updateLoginError(value) {
    return {
        type: types.USERS_UPDATE_LOGINERROR,
        value
    }
}

function updateRegisterError(value) {
    return {
        type: types.USERS_UPDATE_REGISTERERROR,
        value
    }
}

function updateRememberError(value) {
    return {
        type: types.USERS_UPDATE_REMEMBERERROR,
        value
    }
}

function updateRememberSuccess(value) {
    return {
        type: types.USERS_UPDATE_REMEMBERSUCCESS,
        value
    }
}

// Get user data. If it's the current user, update also that variable
export function fetchUser(id) {
    return function(dispatch, getState) {
        dispatch(updateUser({}))
        if (id === global.user.uid) {
            dispatch(updateCurrentUser({}))
        }
        dispatch(updateFetching(true))

        usersRef.doc(id)
            .get()
            .then((d) => {
                dispatch(updateUser(_.merge({ id }, d.data())))
                if (id === global.user.uid) {
                    dispatch(updateCurrentUser(d.data()))
                }
            })
            .catch(err => {
                Alert.alert('Error al cargar el usuario')
                console.error('fetchUser err:', err)
            })
            .finally(_ => 
                dispatch(updateFetching(false))    
            )
    }
}

// Updates user data
export function postUser(user) {
    return function(dispatch, getState) {
        if (!user) {
            return
        }
        
        dispatch(updateFetching(true))
        if (user.avatar && user.avatar.fileName) {
            uploadImage(
                user.avatar.uri, user.avatar.width, user.avatar.height,
                `avatars/${global.user.uid}${user.avatar.fileName}`,
                (snapshot) => {
                    user.avatar = snapshot.downloadURL
                    _updateUser(user, dispatch)
                },
                (error) => {
                    Alert.alert('Error al subir el avatar')
                    console.error('postUser err:', error)
                }
            )
        } else {
            user.avatar = user.avatar && user.avatar.uri ? user.avatar.uri : null
            _updateUser(user, dispatch)
        }     
    }
}

function _updateUser(user, dispatch) {
    const userData = {
        name: user.name,
        bio: user.bio,
        avatar: user.avatar
    }

    usersRef.doc(global.user.uid)
        .get()
        .then((d) => {
            if (d.data()) {
                usersRef.doc(global.user.uid)
                    .update(userData)
                    .then(() => {
                        dispatch(updateCurrentUser(userData))
                        Actions.pop()
                    })
                    .catch(err => {
                        Alert.alert('Error al actualizar el usuario')
                        console.error('postUser err:', err)
                    })
                    .finally(() => {
                        dispatch(updateFetching(false))
                    })
            } else {
                usersRef.doc(global.user.uid)
                    .set(userData)
                    .then(() => {
                        dispatch(updateCurrentUser(userData))
                        Actions.pop()
                    })
                    .catch(err => {
                        Alert.alert('Error al actualizar el usuario')
                        console.error('postUser err:', err)
                    })
                    .finally(() => {
                        dispatch(updateFetching(false))
                    })
            }
        })
        .catch(err => {
            Alert.alert('Error al actualizar el usuario')
            console.error('postUser err:', err)
            dispatch(updateFetching(false))
        })
}

// Checks if email and password are a valid user
export function loginUser(email, password) {
    return function(dispatch, getState) {
        dispatch(updateFetching(true))
        dispatch(updateLoginError(false))

        firebase.auth().signInWithEmailAndPassword(email, password)
                .then((user) => {
                    console.log('User logged in')
                 })
                .catch((err, msg) => {
                    dispatch(updateLoginError(true))
                })
                .finally(() => {
                    dispatch(updateFetching(false))
                })
    }
}

// Creates a user account
export function registerUser(email, password) {
    return function(dispatch, getState) {
        dispatch(updateFetching(true))
        dispatch(updateRegisterError(false))

        firebase.auth().createUserWithEmailAndPassword(email, password)
                .then((user) => {
                    console.log('User registered')
                 })
                .catch((err, msg) => {
                    dispatch(updateRegisterError(true))
                })
                .finally(() => {
                    dispatch(updateFetching(false))
                })
    }
}

// Sends password reset link to a registered user
export function rememberPassword(email) {
    return function(dispatch, getState) {
        dispatch(updateFetching(true))
        dispatch(updateRememberError(false))

        firebase.auth().sendPasswordResetEmail(email)
                .then((user) => {
                    dispatch(updateRememberSuccess(true))
                 })
                .catch((err, msg) => {
                    dispatch(updateRememberError(true))
                    dispatch(updateRememberSuccess(false))
                })
                .finally(() => {
                    dispatch(updateFetching(false))
                })
    }
}
  
export function logoutUser() {
    return function(dispatch, getState) {
        firebase.auth().signOut()
    }
}