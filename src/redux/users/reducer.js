import * as types from './types'

const initialState = {
    currentUser: {},
    user: {},
    isFetching: false,
    isLoginError: false,
    isRegisterError: false,
    isRememberError: false,
    isRememberSuccess: false
}

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case types.USERS_UPDATE_USER:
            return {
                ...state,
                user: action.user
            }

        case types.USERS_UPDATE_CURRENTUSER:
            return {
                ...state,
                currentUser: action.user
            }

        case types.USERS_UPDATE_FETCHING:
            return {
                ...state,
                isFetching: action.value
            }

        case types.USERS_UPDATE_LOGINERROR:
            return {
                ...state,
                isLoginError: action.value
            }

        case types.USERS_UPDATE_REGISTERERROR:
            return {
                ...state,
                isRegisterError: action.value
            }

        case types.USERS_UPDATE_REMEMBERERROR:
            return {
                ...state,
                isRememberError: action.value
            }

        case types.USERS_UPDATE_REMEMBERSUCCESS:
            return {
                ...state,
                isRememberError: action.value
            }

        default:
            return state
    }
}