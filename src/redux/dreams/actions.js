import * as types from './types'
import { Alert } from 'react-native'
import firebase from 'react-native-firebase'
import { Actions } from 'react-native-router-flux'
import { uploadImage } from '../../commons/global'
import _ from 'lodash'

const LIMIT = 10
const dreamsRef = firebase.firestore().collection('dreams')

function updateSearchList(list, total) {
    return {
        type: types.DREAMS_UPDATE_SEARCH_LIST,
        list,
        total
    }
}

function updateUserList(list) {
    return {
        type: types.DREAMS_UPDATE_USER_LIST,
        list
    }
}

function updateFavouriteList(list) {
    return {
        type: types.DREAMS_UPDATE_FAVOURITE_LIST,
        list
    }
}

function updateSharedList(list) {
    return {
        type: types.DREAMS_UPDATE_SHARED_LIST,
        list
    }
}

function updateOwnUserList(list) {
    return {
        type: types.DREAMS_UPDATE_OWN_USER_LIST,
        list
    }
}

function updateOwnFavouriteList(list) {
    return {
        type: types.DREAMS_UPDATE_OWN_FAVOURITE_LIST,
        list
    }
}

function updateOwnSharedList(list) {
    return {
        type: types.DREAMS_UPDATE_OWN_SHARED_LIST,
        list
    }
}

function updateFetching(value) {
    return {
        type: types.DREAMS_UPDATE_FETCHING,
        value
    }
}

function updateOffset(value) {
    return {
        type: types.DREAMS_UPDATE_OFFSET,
        value
    }
}

function updateUserIsCollaborator(value) {
    return {
        type: types.DREAMS_UPDATE_USER_IS_COLLABORATOR,
        value
    }
}

export function initDreamsSearch(category, query) {
    return function(dispatch, getState) {
        dispatch(updateSearchList([], 0))
        dispatch(updateOffset(0))
        dispatch(searchDreams(category, query))
    }
}

export function updateDreamsSearchOffset(category, query) {
    return function(dispatch, getState) {
        const offset = getState().dreams.offset + LIMIT
        dispatch(updateOffset(offset))
        dispatch(searchDreams(category, query))
    }
}

// Search for dreams. Both category and query are optional
export function searchDreams(category, query) {
    return function(dispatch, getState) {
        dispatch(updateFetching(true))

        const { offset, searchList } = getState().dreams

        if (category !== 0) {
            dreamsRef
                .where('category', '==', category)
                .where('state', '==', 'published')
                .orderBy('date', 'desc')
                .get()
                .then((d) => {
                    // Dirty hacks: faux pagination and faux text search
                    const results = d.docs
                                    .map(d => _.merge({ id: d.id }, d.data()))
                                    .filter(d => _filterDream(d, query))

                    const total = results.length
                    const data = results.slice(offset, offset + LIMIT)
                    const newList = [...searchList, ...data]

                    dispatch(updateSearchList(newList, total))
                })
                .catch(err => {
                    console.error('searchDreams err:', err)
                })
                .finally(_ => 
                    dispatch(updateFetching(false))    
                )
        } else {
            dreamsRef
                .where('state', '==', 'published')
                .orderBy('date', 'desc')
                .get()
                .then((d) => {
                    // Dirty hacks: faux pagination and faux text search
                    const results = d.docs
                                    .map(d => _.merge({ id: d.id }, d.data()))
                                    .filter(d => _filterDream(d, query))

                    const total = results.length
                    const data = results.slice(offset, offset + LIMIT)
                    const newList = [...searchList, ...data]

                    dispatch(updateSearchList(newList, total))
                })
                .catch(err => {
                    console.error('searchDreams err:', err)
                })
                .finally(_ => 
                    dispatch(updateFetching(false))    
                )
        }
    }
}

// Gets dreams created by a user
export function fetchUserDreams(uid) {
    return function(dispatch, getState) {
        if (uid === global.user.uid) {
            dispatch(updateOwnUserList([], 0))
        } else {
            dispatch(updateUserList([], 0))
        }
        dispatch(updateFetching(true))

        dreamsRef
            .where('user', '==', uid)
            .orderBy('date', 'desc')
            .get()
            .then((d) => {
                const results = d.docs
                                .map(d => _.merge({ id: d.id }, d.data()))

                if (uid === global.user.uid) {
                    dispatch(updateOwnUserList(results))
                } else {
                    dispatch(updateUserList(results))
                }
            })
            .catch(err => {
                console.error('fetchUserDreams err:', err)
            })
            .finally(_ => 
                dispatch(updateFetching(false))    
            )
    }
}

// Get dreams favourited by a user
export function fetchFavouriteDreams(uid) {
    return function(dispatch, getState) {
        if (uid === global.user.uid) {
            dispatch(updateOwnFavouriteList([]))
        } else {
            dispatch(updateFavouriteList([]))
        }
        dispatch(updateFetching(true))

        dreamsRef
            .where('favourites.' + uid, '==', true)
            .get()
            .then((d) => {
                const results = d.docs
                                .map(d => _.merge({ id: d.id }, d.data()))

                if (uid === global.user.uid) {
                    dispatch(updateOwnFavouriteList(results))
                } else {
                    dispatch(updateFavouriteList(results))
                }
            })
            .catch(err => {
                console.error('fetchFavouriteDreams err:', err)
            })
            .finally(_ => 
                dispatch(updateFetching(false))    
            )
    }
}

// Gets dreams that a user has participated in
export function fetchSharedDreams(uid) {
    return function(dispatch, getState) {
        if (uid === global.user.uid) {
            dispatch(updateOwnSharedList([]))
        } else {
            dispatch(updateSharedList([]))
        }
        dispatch(updateFetching(true))

        dreamsRef
            .where('users.' + uid, '==', true)
            .get()
            .then((d) => {
                const results = d.docs
                                .map(d => _.merge({ id: d.id }, d.data()))
                
                if (uid === global.user.uid) {
                    dispatch(updateOwnSharedList(results))
                } else {
                    dispatch(updateSharedList(results))
                }
            })
            .catch(err => {
                console.error('fetchSharedDreams err:', err)
            })
            .finally(_ => 
                dispatch(updateFetching(false))    
            )
    }
}

// Mark dream as favourite
export function favouriteDream(dream, fav) {
    return function(dispatch, getState) {
        dispatch(updateFetching(true))

        dreamsRef
            .doc(dream.id)
            .update({
                favourites: fav
            })
            .then((d) => {
                dream.favourites = fav
                dispatch(fetchFavouriteDreams())
            })
            .catch((error) => {
                console.error('favouriteDream err:', error)
            })
            .finally(_ => 
                dispatch(updateFetching(false))    
            )
    }
}

export function updateDreamData(dream) {
    return function(dispatch, getState) {
        const collaborator = (dream && dream.users && dream.users[global.user.uid])
        dispatch(updateUserIsCollaborator(collaborator))
    }
}

// Collaborate... or not :)
export function helpDream(dream, users) {
    return function(dispatch, getState) {
        dispatch(updateFetching(true))

        dreamsRef
            .doc(dream.id)
            .update({
                users: users
            })
            .then((d) => {
                dream.users = users
                const collaborator = (dream && dream.users && dream.users[global.user.uid])
                dispatch(updateUserIsCollaborator(collaborator))
                dispatch(fetchSharedDreams())

                // Exit from chat
                if (dream.user !== global.user.uid && !users[global.user.uid]) {
                    Actions.pop()
                }
            })
            .catch((error) => {
                console.error('helpDream err:', error)
            })
            .finally(_ => 
                dispatch(updateFetching(false))    
            )
    }
}

// Post dream to firebase (in two stages)
export function postDream(dream, photo) {
    return function(dispatch, getState) {
        dispatch(updateFetching(true))
        dream.date = firebase.firestore.FieldValue.serverTimestamp()
        dream.user = global.user.uid

        if (photo && photo.fileName) {
            dispatch(_uploadDreamImage(dream, photo, _postDream))
        } else {
            dispatch(_postDream(dream))
        }
    }
}

function _postDream(dream) {
    return function(dispatch, getState) {
        dreamsRef.add(dream)
            .then((d) => {
                dispatch(fetchUserDreams(global.user.uid))
                Actions.pop()
            })
            .catch((error) => {
                Alert.alert('Error al crear el sueño')
                dispatch(updateFetching(false))
            })
    }
}

// Updates dream in firebase (in two stages)
export function updateDream(dream, photo) {
    return function(dispatch, getState) {
        dispatch(updateFetching(true))

        if (photo && photo.fileName) {
            dispatch(_uploadDreamImage(dream, photo, _updateDream))
        } else {
            dream.image = (photo && photo.uri) ? photo.uri : null
            dispatch(_updateDream(dream))
        }
    }
}

function _updateDream(dream) {
    return function(dispatch, getState) {
        let dreamClone = _.clone(dream)
        delete dreamClone['id']

        dreamsRef
            .doc(dream.id)
            .update(dreamClone)
            .then((d) => {
                dispatch(fetchUserDreams(global.user.uid))
                Actions.pop()
                Actions.refresh({ dream: dream, key: Math.random() })
            })
            .catch((error) => {
                Alert.alert('Error al actualizar el sueño')
                dispatch(updateFetching(false))
            })
    }
}

function _uploadDreamImage(dream, photo, action) {
    return function(dispatch, getState) {
        uploadImage(
            photo.uri, photo.width, photo.height, 
            `dreams/${new Date().getTime()}${photo.fileName}`,
            (snapshot) => {
                dream.image = snapshot.downloadURL
                dispatch(action(dream))
            },
            (error) => {
                dispatch(updateFetching(false))
                Alert.alert('Error al subir la imagen')
            }
        )
    }
}

function _filterDream(d, query) {
    const search = query.trim().toLowerCase()
    return search === '' || 
        (d.name && d.name.toLowerCase().includes(search)) || 
        (d.description && d.description.toLowerCase().includes(search)) || 
        (d.location && d.location.toLowerCase().includes(search))
}