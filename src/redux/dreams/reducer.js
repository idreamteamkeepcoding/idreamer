import * as types from './types'

const initialState = {
    searchList: [],
    userList: [],
    favouriteList: [],
    sharedList: [],
    ownUserList: [],
    ownFavouriteList: [],
    ownSharedList: [],
    total: 0,
    isFetching: false,
    offset: 0,
    userIsCollaborator: false
}

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case types.DREAMS_UPDATE_SEARCH_LIST:
            return {
                ...state,
                searchList: action.list,
                total: action.total
            }

        case types.DREAMS_UPDATE_USER_LIST:
            return {
                ...state,
                userList: action.list
            }

        case types.DREAMS_UPDATE_FAVOURITE_LIST:
            return {
                ...state,
                favouriteList: action.list
            }

        case types.DREAMS_UPDATE_SHARED_LIST:
            return {
                ...state,
                sharedList: action.list
            }

        case types.DREAMS_UPDATE_OWN_USER_LIST:
            return {
                ...state,
                ownUserList: action.list
            }

        case types.DREAMS_UPDATE_OWN_FAVOURITE_LIST:
            return {
                ...state,
                ownFavouriteList: action.list
            }

        case types.DREAMS_UPDATE_OWN_SHARED_LIST:
            return {
                ...state,
                ownSharedList: action.list
            }

        case types.DREAMS_UPDATE_FETCHING:
            return {
                ...state,
                isFetching: action.value
            }

        case types.DREAMS_UPDATE_OFFSET:
            return {
                ...state,
                offset: action.value
            }

        case types.DREAMS_UPDATE_USER_IS_COLLABORATOR:
            return {
                ...state,
                userIsCollaborator: action.value
            }

        default:
            return state
    }
}