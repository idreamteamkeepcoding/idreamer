import * as types from './types'
import { Alert } from 'react-native'
import firebase from 'react-native-firebase'
import _ from 'lodash'

const searchesRef = firebase.firestore().collection('searches')

function updateList(list) {
    return {
        type: types.SEARCHES_UPDATE_LIST,
        list
    }
}

function updateFetching(value) {
    return {
        type: types.SEARCHES_UPDATE_FETCHING,
        value
    }
}

// Gets search list from current user
export function fetchSearchesList() {
    return function(dispatch, getState) {
        dispatch(updateList([], 0))
        dispatch(updateFetching(true))

        searchesRef
            .where('user', '==', global.user.uid)
            .orderBy('date', 'desc')
            .get()
            .then((d) => {
                const list = d.docs
                                .map(d => _.merge({ id: d.id }, d.data()))

                dispatch(updateList(list))
            })
            .catch(err => {
                console.error('fetchSearchesList err:', err)
            })
            .finally(_ => 
                dispatch(updateFetching(false))    
            )
    }
}

// Saves dream search for current user
export function postSearch(search) {
    return function(dispatch, getState) {
        if (!search) {
            return
        }
  
        dispatch(updateFetching(true))
        searchesRef
            .add({
                query: search.query,
                category: search.category,
                user: global.user.uid,
                date: firebase.firestore.FieldValue.serverTimestamp()
            })
            .then(() => {
                dispatch(fetchSearchesList())
                Alert.alert('Búsqueda guardada correctamente')
            })
            .catch(err => {
                console.error('postSearch err:', err)
                dispatch(updateFetching(false))
            })
    }
}

// Deletes dream search for current user
export function deleteSearch(id) {
    return function(dispatch, getState) {
        if (!id) {
            return
        }
  
        dispatch(updateFetching(true))
        searchesRef
            .doc(id)
            .delete()
            .then(() => {
                dispatch(fetchSearchesList())
            })
            .catch(err => {
                Alert.alert('Error al eliminar la búsqueda')
                console.error('deleteSearch err:', err)
                dispatch(updateFetching(false))
            })
    }
}
  