import * as types from './types'

const initialState = {
    messageList: [],
    imageList: [],
    isFetching: false
}

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case types.CHATS_UPDATE_MESSAGE_LIST:
            return {
                ...state,
                messageList: action.list
            }

        case types.CHATS_UPDATE_IMAGE_LIST:
            return {
                ...state,
                imageList: action.list
            }

        case types.CHATS_UPDATE_FETCHING:
            return {
                ...state,
                isFetching: action.value
            }

        default:
            return state
    }
}