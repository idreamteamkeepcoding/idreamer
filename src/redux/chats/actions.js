import * as types from './types'
import { Alert } from 'react-native'
import firebase from 'react-native-firebase'
import { uploadImage } from '../../commons/global'
import _ from 'lodash'

const chatsRef = firebase.firestore().collection('chats')
let unsuscriber = null

function updateMessageList(list) {
    return {
        type: types.CHATS_UPDATE_MESSAGE_LIST,
        list
    }
}

function updateImageList(list) {
    return {
        type: types.CHATS_UPDATE_IMAGE_LIST,
        list
    }
}

function updateFetching(value) {
    return {
        type: types.CHATS_UPDATE_FETCHING,
        value
    }
}

// Inits chat document (if empty) and keeps listening for new messages
export function initChat(dream) {
    return function(dispatch, getState) {
        dispatch(updateMessageList([]))
        dispatch(updateImageList([]))
        dispatch(updateFetching(true))
        
        chatsRef.doc(dream.id).get()
            .then((d) => {
                const data = d.data()
                if (data && data.messages) {
                    dispatch(updateMessageList(data.messages.reverse()))
                    dispatch(updateImageList(_getImages(data.messages)))
                    dispatch(updateFetching(false))
                } else {
                    chatsRef.doc(dream.id).set({
                        messages: [],
                        images: []
                    })
                    .catch((error) => {
                        Alert.alert('Error al inicializar el chat')
                    })
                    .finally(() => {
                        dispatch(updateFetching(false))
                    })
                }
            })
            .catch((error) => {
                Alert.alert('Error al obtener los mensajes')
                dispatch(updateFetching(false))
            })
        
        unsuscriber = chatsRef.doc(dream.id).onSnapshot((d) => {
            const data = d.data()
            if (data && data.messages) {
                dispatch(updateMessageList(data.messages.reverse()))
                dispatch(updateImageList(_getImages(data.messages)))
            } else {
                dispatch(updateMessageList([]))
                dispatch(updateImageList([]))
            }
        })
    }
}

// Extract images from messages
function _getImages(messages) {
    return messages
            .filter(m => m.image && m.image !== '')
            .map(m => { 
                return { 
                    url: m.image, 
                    width: m.width, 
                    height: m.height 
                }
            })
            .reverse()
}

// Send message to chat
export function sendMessage(message, dream) {
    return function(dispatch, getState) {
        const user = getState().users.currentUser
        const userName = _.get(user, 'name', null)
        const userAvatar = _.get(user, 'avatar', null)
        chatsRef.doc(dream.id).update({
            messages: firebase.firestore.FieldValue.arrayUnion({
                message,
                date: new Date(), // firebase.firestore.FieldValue.serverTimestamp() can only be used on set and update
                user: {
                    id: global.user.uid,
                    name: userName,
                    avatar: userAvatar
                }
            })
        })
            .catch((error) => {
                Alert.alert('Error al enviar el mensaje')
            })
    }
}

// Send image to chat
export function sendImage(image, dream) {
    return function(dispatch, getState) {
        const user = getState().users.currentUser
        const userName = _.get(user, 'name', null)
        const userAvatar = _.get(user, 'avatar', null)
        
        uploadImage(
            image.uri, image.width, image.height,
            `chat/${global.user.uid}${new Date().getTime()}`,
            (snapshot, width, height) => {
                chatsRef.doc(dream.id).update({
                    messages: firebase.firestore.FieldValue.arrayUnion({
                        image: snapshot.downloadURL,
                        height,
                        width,
                        date: new Date(), // firebase.firestore.FieldValue.serverTimestamp() can only be used on set and update
                        user: {
                            id: global.user.uid,
                            name: userName,
                            avatar: userAvatar
                        }
                    })
                })
                    .catch((error) => {
                        Alert.alert('Error al enviar el mensaje')
                    })
            },
            (error) => {
                Alert.alert('Error al subir la imagen')
            })
    }
}

// On exit, remove listener
export function unsuscribeMessages() {
    return function(dispatch, getState) {
        if (unsuscriber) {
            unsuscriber()
        }
    }
}

