import React from 'react'
import { 
    View, 
    ScrollView, 
    SafeAreaView, 
    Text, 
    FlatList, 
    ActivityIndicator, 
    Image, 
    Switch, 
    Platform, 
    PermissionsAndroid,
    Alert
} from 'react-native'
import { Input, Button, Category } from '../../widgets'
import ImagePicker from 'react-native-image-picker'
import SegmentedControlTab from 'react-native-segmented-control-tab'
import styles from './styles'
import * as colors from '../../commons/colors'
import * as dreamStates from '../../commons/dreamStates'
import firebase from 'react-native-firebase'
import Geolocation from 'react-native-geolocation-service'
import _ from 'lodash'

export default class DreamForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            photo: props.dream && props.dream.image ? { uri: props.dream.image } : null,
            name: _.get(props, 'dream.name', ''),
            nameError: '',
            description: _.get(props, 'dream.description', ''),
            descriptionError: '',
            location: _.get(props, 'dream.location', ''),
            locationError: '',
            latitude: _.get(props, 'dream.coordinates.latitude', null),
            longitude: _.get(props, 'dream.coordinates.longitude', null),
            savePosition: props.dream && props.dream.coordinates ? true : false,
            category: _.get(props, 'dream.category', 0),
            categoryError: '',
            stateIndex: this._getStateIndex(_.get(props, 'dream.state', 'published'))
        }
        this.categories = global.categories.filter(c => c.id !== 0)
    }

    _onCategoryTapped = category => {
        this.setState({ 
            category: category.id
        })
    }

    _keyExtractor = (item, index) => `${item.id}`

    _renderCategory = ({ item, index }) => (
        <Category 
            icon={item.icon}
            label={item.name}
            selected={item.id === this.state.category}
            onPress={() => this._onCategoryTapped(item)}
        />
    )

    _addImage = () => {
        ImagePicker.launchImageLibrary(global.imagePickerOptions, (response) => {
            if (response.uri) {
                this.setState({ photo: response })
            }
        })
    }

    _removeImage = () => {
        this.setState({
            photo: null
        })
    }

    _onChangeCoordinatesSwitch = (active) => {
        this.setState({
            savePosition: active
        })
        if (active) {
            if (Platform.OS === 'ios') {
                this._geolocateUser()
            } else {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                    .then(granted => {
                        if (granted) {
                            this._geolocateUser()
                        } else {
                            Alert.alert('No se pudo obtener la posición actual')
                        }
                    })
                    .catch(error => {
                        Alert.alert('Error al obtener la posición actual')
                        console.error(error)
                    })
            } 
        }
    }

    _geolocateUser = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                })
            },
            (error) => {
                Alert.alert('Error al obtener la posición actual')
                console.error(error)
            },
            { 
                enableHighAccuracy: true, 
                timeout: 20000, 
                maximumAge: 1000
            }
        )
    }

    _getStateIndex = (state) => {
        switch (state) {
            case dreamStates.STATE_PUBLISHED:
                return 0
            
            case dreamStates.STATE_CANCELED:
                return 1
    
            case dreamStates.STATE_COMPLETED:
                return 2
    
            default:
                return 0
        }
    }

    _getState = (stateIndex) => {
        switch (stateIndex) {
            case 0:
                return dreamStates.STATE_PUBLISHED
            
            case 1:
                return dreamStates.STATE_CANCELED
    
            case 2:
                return dreamStates.STATE_COMPLETED
    
            default:
                return dreamStates.STATE_PUBLISHED
        }
    }

    _changeStateIndex = (stateIndex) => {
        this.setState({
            stateIndex
        })
    }

    _submitDream = () => {
        const { photo, name, description, location, savePosition, latitude, longitude, category, stateIndex } = this.state

        let error = false
        this.setState({
            nameError: '',
            descriptionError: '',
            locationError: '',
            categoryError: ''
        })
        if (name.trim() === '') {
            this.setState({ nameError: 'El nombre no puede estar vacío' })
            error = true
        }
        if (description.trim() === '') {
            this.setState({ descriptionError: 'La descripción no puede estar vacía' })
            error = true
        }
        if (location.trim() === '') {
            this.setState({ locationError: 'La localización no puede estar vacía' })
            error = true
        }
        if (category === 0) {
            this.setState({ categoryError: 'Debes seleccionar una categoría' })
            error = true
        }

        if (!error) {
            const coordinates = savePosition && latitude && longitude ? new firebase.firestore.GeoPoint(latitude, longitude) : null
            const dream = _.merge({}, this.props.dream, {
                name,
                description,
                location,
                category,
                coordinates,
                state: this._getState(stateIndex)
            })
            this.props.onSubmit(dream, photo)
        }
    }

    render() {
        const { 
            photo,
            name, 
            nameError, 
            description, 
            descriptionError, 
            location, 
            locationError,
            savePosition,
            categoryError,
            stateIndex
        } = this.state

        const { saving } = this.props
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.form}>
                    <Text style={styles.label}>Imagen</Text>
                    {
                        photo && (
                        <View>
                            <Image
                                source={{ uri: photo.uri }}
                                style={styles.image}
                            />
                            <Button 
                                label="Quitar imagen"
                                icon="minus"
                                onPress={() => this._removeImage()}
                            />
                        </View>
                        )
                    }
                    {
                        !photo &&
                        <Button 
                            label="Añadir imagen"
                            icon="plus"
                            onPress={() => this._addImage()}
                        />
                    } 
                    {
                        this.props.dream &&
                        <View>
                            <Text style={styles.label}>Estado</Text>
                            <SegmentedControlTab
                                tabStyle={styles.state}
                                values={['Publicado', 'Cancelado', 'Completado']}
                                selectedIndex={stateIndex}
                                onTabPress={this._changeStateIndex}
                            />
                        </View>
                    }
                    <Input
                        label="Nombre"
                        placeholder="¿Cuál es tu sueño?"
                        icon="cloud"
                        value={name}
                        error={nameError}
                        onChangeText={name => this.setState({ name })}
                    />
                    <Input
                        label="Descripción"
                        placeholder="¿En qué consiste?"
                        icon="font"
                        value={description}
                        error={descriptionError}
                        onChangeText={description => this.setState({ description })}
                        multiline={true}
                        inputStyle={{ height: 200 }}
                    />
                    <Input
                        label="Localización"
                        placeholder="¿Dónde se tiene que cumplir?"
                        icon="map-marker-alt"
                        value={location}
                        error={locationError}
                        onChangeText={location => this.setState({ location })}
                    />
                    <Text style={styles.label}>Mapa</Text>
                    <View style={styles.coordinates}>
                        <Switch
                            onValueChange={(value) => this._onChangeCoordinatesSwitch(value)}
                            value={savePosition}
                            style={styles.coordinatesSwitch}
                            trackColor={{
                                false: colors.background,
                                true: colors.main
                            }}
                        />
                        <Text>Mostrar con la posición actual</Text>
                    </View>
                    <Text style={styles.label}>Categoría</Text>
                    <FlatList
                        style={styles.categories}
                        data={this.categories}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderCategory}
                        horizontal={true}
                        extraData={this.state}
                        showsHorizontalScrollIndicator={false}
                    />
                    {
                        categoryError !== '' &&
                        <Text style={styles.error}>{categoryError}</Text>
                    }
                </ScrollView>
                <View style={styles.bottom}>
                    {
                        !saving &&
                        <Button 
                            label={this.props.dream ? 'Guardar cambios' : 'Publicar sueño'}
                            icon="check"
                            onPress={() => this._submitDream()}
                        />
                    }
                    {
                        saving &&
                        <ActivityIndicator 
                            color={colors.main} 
                            size="large" 
                        />
                    }
                </View>
            </SafeAreaView>
        )
    }
}

