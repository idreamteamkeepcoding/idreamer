import { StyleSheet, Dimensions } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    form: {
        marginHorizontal: 16,
        marginBottom: 90
    },
    image: {
        width: '100%',
        height: 200,
        resizeMode: 'cover',
        marginVertical: 10
    },
    label: {
        color: colors.main,
        marginBottom: 4,
        marginTop: 10,
        fontSize: 15,
        fontFamily: 'Poppins-Bold',
    },
    state: {
        color: colors.main,
        backgroundColor: colors.lightWhite,
        marginBottom: 10
    },
    error: {
        color: colors.error,
        fontFamily: 'Poppins-Bold',
        textAlign: 'center',
        marginVertical: 5
    },
    coordinates: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-start'
    },
    coordinatesSwitch: {
        marginRight: 12
    },
    bottom: {
        position: 'absolute',
        width: 200,
        bottom: 24,
        left: (Dimensions.get('window').width / 2) - 100,
        justifyContent: 'center'
    }
})