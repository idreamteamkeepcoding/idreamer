import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    button: {
        padding: 10,
        backgroundColor: colors.main,
        borderWidth: 1,
        borderColor: colors.main,
        borderRadius: 24,
        justifyContent: 'center',
        marginVertical: 10
    },
    buttonWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    icon: {
        color: colors.white,
        marginRight: 8
    },
    label: {
        textAlign: 'center',
        color: colors.white,
        fontSize: 16
    }
})