import React from 'react'
import { TouchableOpacity, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import styles from './styles'

export default class Button extends React.Component {
    static defaultProps = {
        label: '',
        icon: '',
        iconSize: 24,
        iconSolid: false,
        onPress: () => {},
        buttonStyle: {},
        labelStyle: {}
    }

    render() {
        const { label, icon, iconSize, iconSolid, onPress, buttonStyle, labelStyle } = this.props
        return (
            <TouchableOpacity style={[styles.button, buttonStyle]} onPress={onPress}>
                <View style={styles.buttonWrapper}>
                    {
                        icon !== '' &&
                        <Icon name={icon} style={styles.icon} solid={iconSolid} size={iconSize} />
                    }
                    {
                        label !== '' &&
                        <Text style={[styles.label, labelStyle]}>{label}</Text>
                    }
                </View>
            </TouchableOpacity>
        )
    }
}