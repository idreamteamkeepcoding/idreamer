import React from 'react'
import { TouchableOpacity, Text, Image, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import styles from './styles'

export default class SearchCell extends React.Component {
    static defaultProps = {
        query: '',
        categoryId: 0,
        onPress: () => {},
        onLongPress: () => {},
        labelStyle: {}
    }

    render() {
        const { query, categoryId, onPress, onLongPress, labelStyle } = this.props
        const category = global.categories.find(c => c.id === categoryId)
        return (
            <TouchableOpacity 
                style={styles.container} 
                onPress={onPress}
                onLongPress={onLongPress}
            >
                <View style={styles.content}>
                {
                    query !== '' &&
                    <Text style={[styles.label, labelStyle]}>{query}</Text>
                }
                {
                    query === '' &&
                    <Text style={styles.noLabel}>(Sin texto)</Text>
                }
                {
                    category &&
                    <View style={styles.category}>
                        <Icon style={styles.icon} name={category.icon} size={16} />
                        <Text style={styles.categoryName}>{category.name}</Text>
                    </View>
                }
                </View>
                <View style={styles.anchor}>
                    <Icon style={styles.anchorIcon} name="chevron-right" size={24} />
                </View>
            </TouchableOpacity>
        )
    }
}