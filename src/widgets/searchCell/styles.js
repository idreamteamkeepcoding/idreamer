import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        padding: 10,
        backgroundColor: colors.lightWhite,
        borderBottomColor: colors.border,
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    content: {
        flex: 1,
        flexDirection: 'column'
    }, 
    label: {
        color: colors.darkGrey,
        fontSize: 16,
        fontFamily: 'Poppins-Bold'
    },
    noLabel: {
        color: colors.grey,
        fontSize: 16
    },
    category: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    icon: {
        color: colors.grey,
        width: 32
    },
    categoryName: {
        fontSize: 14,
        color: colors.grey
    },
    anchor: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    anchorIcon: {
        color: colors.lightGrey
    }
})