import React from 'react'
import { View, Text } from 'react-native'
import styles from './styles'
import * as colors from '../../commons/colors'

export default class Message extends React.Component {
    static defaultProps = {
        error: false,
        message: ''
    }

    render() {
        const { error, message } = this.props
        return (
            <View>
            {
                message !== '' &&
                <View style={[styles.messageView, error ? styles.errorView : {}]}>
                    <Text style={[styles.message, { color: error ? colors.error : colors.message }]}>{message}</Text>
                </View>
            }
            </View>
        )
    }
}