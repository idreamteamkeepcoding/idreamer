import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    messageView: {
        borderWidth: 1,
        borderColor: colors.messageBorder,
        borderRadius: 8,
        backgroundColor: colors.messageBg,
        padding: 16,
        marginTop: 16
    },
    errorView: {
        borderColor: colors.errorBorder,
        backgroundColor: colors.errorBg,
    },
    message: {
        textAlign: 'center',
        fontSize: 14
    }
})