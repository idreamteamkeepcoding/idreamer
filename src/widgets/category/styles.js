import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: colors.main,
        borderWidth: 1,
        borderRadius: 14,
        height: 28,
        marginRight: 8
    },
    icon: {
        color: colors.main,
        paddingHorizontal: 10
    },
    label: {
        color: colors.main,
        fontSize: 14,
        paddingRight: 10
    }
})