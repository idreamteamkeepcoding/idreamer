import React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import styles from './styles'
import * as colors from '../../commons/colors'

export default class Category extends React.Component {
    static defaultProps = {
        label: '',
        icon: '',
        selected: false,
        style: {},
        onPress: () => {}
    }

    render() {
        const { 
            label, 
            icon,
            selected,
            style,
            onPress 
        } = this.props
        return (
            <TouchableOpacity 
                style={[styles.container, { backgroundColor: selected ? colors.main : colors.lightWhite }, style]} 
                onPress={onPress}>
                {
                    icon ? <Icon style={[styles.icon, selected ? { color: colors.white } : {}]} name={icon} size={16} /> : null
                }
                <Text style={[styles.label, selected ? { color: colors.white } : {}]}>{label}</Text>
            </TouchableOpacity>
        )
    }
}