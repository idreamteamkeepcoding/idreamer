import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { User } from '../'
import { CachedImage } from 'react-native-cached-image'
import moment from 'moment'
import styles from './styles'
import * as colors from '../../commons/colors'
import { Actions } from 'react-native-router-flux'
import _ from 'lodash'

export default class ChatMessage extends React.Component {
    static defaultProps = {
        message: {},
        onUserLongPress: () => {},
        onImagePress: () => {}
    }

    _onUserPressed = () => {
        Actions.UserDetails({
            user: this.props.message.user
        })
    }

    render() {
        const { message, onUserLongPress, onImagePress } = this.props
        const isMine = message.user.id === global.user.uid
        
        return (
            <View style={styles.container}>
                <View style={isMine ? styles.messageMine : styles.messageOther}>
                    {
                        !isMine &&
                        <User user={message.user} onPress={() => this._onUserPressed()} onLongPress={onUserLongPress} />
                    }
                    {
                        message.message && message.message !== '' &&
                        <View style={[styles.message, { backgroundColor: isMine ? colors.lightMain : colors.lightWhite, alignSelf: isMine ? 'flex-end' : 'flex-start' }]}>
                            <Text>{message.message}</Text>
                        </View>
                    }
                    {
                        message.image && message.image !== '' &&
                        <TouchableOpacity style={[styles.imageContainer, { alignSelf: isMine ? 'flex-end' : 'flex-start' }]} onPress={() => onImagePress(message.image)}>
                            <CachedImage style={styles.image} source={{ uri: message.image  }} />
                        </TouchableOpacity>
                    }
                    <Text style={styles.date}>{moment(message.date.toDate()).fromNow()}</Text>
                </View>
            </View>
        )
    }
}