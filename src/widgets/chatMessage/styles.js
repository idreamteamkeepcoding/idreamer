import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    messageOther: {
        alignItems: 'flex-start'
    },
    messageMine: {
        alignItems: 'flex-end'
    },
    message: {
        maxWidth: '90%',
        marginVertical: 4,
        padding: 10,
        borderRadius: 4,
        shadowColor: colors.grey,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    imageContainer: {
        width: 200,
        height: 200,
        backgroundColor: colors.white,
        marginVertical: 4,
        borderRadius: 4,
        shadowColor: colors.grey,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    image: {
        width: '100%',
        height: '100%',
        borderRadius: 4,
        resizeMode: 'cover'
    },
    date: {
        color: colors.grey,
        fontSize: 12
    }
})