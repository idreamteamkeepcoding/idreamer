import React from 'react'
import { TouchableOpacity, Text, View } from 'react-native'
import { CachedImage } from 'react-native-cached-image'
import Icon from 'react-native-vector-icons/FontAwesome5'
import styles from './styles'
import * as colors from '../../commons/colors'
import _ from 'lodash'

export default class User extends React.Component {
    static defaultProps = {
        user: {},
        loading: false,
        style: {},
        onPress: () => {},
        onLongPress: () => {}
    }

    render() {
        const { 
            user, 
            loading,
            style,
            onPress,
            onLongPress
        } = this.props

        const userName = _.get(user, 'name', global.ANONYMOUS)
        const userAvatar = _.get(user, 'avatar', null)

        return (
            <TouchableOpacity style={style} onPress={onPress} onLongPress={onLongPress}>
                {
                    loading &&
                    <View style={styles.author}>
                        <View style={styles.avatar} />
                        <View style={styles.authorNameLoading} />
                    </View>
                }
                {
                    !loading &&
                    <View style={styles.author}>
                        {
                            userAvatar &&
                            <CachedImage style={styles.avatar} source={{ uri: userAvatar }} />
                        }
                        {  
                            !userAvatar &&
                            <View style={styles.avatar}>
                                <Icon name="user" color={colors.white} size={16} />
                            </View>
                        }                                                       
                        <Text style={styles.authorName}>{userName}</Text>
                    </View>
                }
            </TouchableOpacity>
        )
    }
}