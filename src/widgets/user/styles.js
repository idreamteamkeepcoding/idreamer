import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

const HEIGHT = 26

export default StyleSheet.create({
    author: {
        flexDirection: 'row',
        backgroundColor: colors.lightGrey,
        borderRadius: HEIGHT / 2,
        height: HEIGHT,
        alignSelf: 'flex-start',
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        width: HEIGHT,
        height: HEIGHT,
        borderRadius: HEIGHT / 2,
        backgroundColor: colors.grey,
        alignItems: 'center',
        justifyContent: 'center'
    },
    authorName: {
        color: colors.darkGrey,
        fontSize: HEIGHT / 2,
        paddingHorizontal: HEIGHT / 2
    },
    authorNameLoading: {
        width: 120,
        height: HEIGHT,
        backgroundColor: colors.lightGrey,
        borderRadius: HEIGHT / 2
    }
})