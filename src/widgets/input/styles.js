import { StyleSheet, Platform } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        marginVertical: 10 
    },
    label: {
        color: colors.main,
        marginBottom: 4,
        fontSize: 15,
        fontFamily: 'Poppins-Bold'
    },
    iconContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: colors.inputBg,
        borderColor: colors.inputBg,
        borderWidth: 1,
        borderRadius: 8,
        height: 32
    },
    icon: {
        color: colors.main,
        paddingVertical: 5,
        paddingLeft: 8,
        paddingRight: 2,
        width: 36
    },
    input: {
        flex: 1,
        color: colors.darkMain,
        fontSize: 14,
        borderRadius: 8,
        paddingVertical: 6,
        paddingHorizontal: 6,
        textAlignVertical: 'top',
        fontFamily: 'Poppins-Regular'
    },
    error: {
        color: colors.error,
        fontFamily: 'Poppins-Bold',
        textAlign: 'center',
        marginVertical: 5
    }
})