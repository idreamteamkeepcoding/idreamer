import React from 'react'
import { View, TextInput, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import styles from './styles'
import * as colors from '../../commons/colors'

export default class Input extends React.Component {
    static defaultProps = {
        label: '',
        value: '',
        error: '',
        icon: '',
        secureTextEntry: false,
        keyboardType: 'default',
        placeholder: '',
        placeholderTextColor: 'rgba(50, 60, 70, 0.6)',
        selectionColor: colors.main,
        containerStyle: {},
        labelStyle: {},
        inputStyle: {},
        errorStyle: {},
        multiline: false,
        autoCorrect: false,
        onChangeText: () => {},
        onSubmitEditing: () => {}
    }

    render() {
        const { 
            label, 
            value, 
            error, 
            icon,
            secureTextEntry,
            keyboardType, 
            placeholder, 
            placeholderTextColor, 
            selectionColor,
            containerStyle, 
            labelStyle, 
            inputStyle, 
            errorStyle, 
            multiline,
            autoCorrect,
            onChangeText,
            onSubmitEditing 
        } = this.props
        return (
            <View style={[styles.container, containerStyle]}>
                {
                    label ? <Text style={[styles.label, labelStyle]}>{label}</Text> : null
                }
                <View style={[styles.iconContainer, inputStyle]}>
                    {
                        icon ? <Icon style={styles.icon} name={icon} size={20} /> : null
                    }
                    <TextInput 
                        style={[styles.input, inputStyle]}
                        onChangeText={onChangeText}
                        onSubmitEditing={onSubmitEditing}
                        value={value}
                        secureTextEntry={secureTextEntry}
                        keyboardType={keyboardType}
                        autoCorrect={autoCorrect}
                        placeholder={placeholder} 
                        placeholderTextColor={placeholderTextColor}
                        selectionColor={selectionColor}
                        underlineColorAndroid="transparent"
                        multiline={multiline}
                        autoCapitalize="none"
                    />
                </View>
                {
                    error ? <Text style={[styles.error, errorStyle]}>{error}</Text> : null
                }
            </View>
        )
    }
}