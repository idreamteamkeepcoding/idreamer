import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        borderBottomColor: colors.border,
        borderBottomWidth: 0.5,
        backgroundColor: colors.lightWhite
    },
    image: {
        width: 120,
        height: '100%',
        resizeMode: 'cover'
    },
    content: {
        flex: 1,
        padding: 8
    },
    labelView: {
        flex: 1
    },
    label: {
        fontSize: 14,
        lineHeight: 16,
        fontFamily: 'Poppins-Bold'
    },
    linearGradient: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    metadataView: {
        position: 'absolute',
        right: 8,
        bottom: 4
    },
    favouritesView: {
        position: 'absolute',
        bottom: 4,
        left: 8
    }, 
    favourites: {
        flexDirection: 'row'
    },
    favouritesIcon: {
        marginTop: 2.5,
        color: colors.grey,
        marginRight: 4
    },
    favouritesText: {
        fontSize: 11,
        color: colors.grey
    },
    metadata: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }, 
    locationText: {
        fontSize: 11,
        fontFamily: 'Poppins-Bold',
        color: colors.grey,
        marginRight: 4
    },
    bull: {
        fontSize: 11,
        color: colors.lightGrey,
        marginRight: 4
    },
    dateText: {
        fontSize: 11,
        color: colors.grey
    }
})