import React from 'react'
import { TouchableOpacity, Text, View } from 'react-native'
import { CachedImage } from 'react-native-cached-image'
import Icon from 'react-native-vector-icons/FontAwesome5'
import LinearGradient from 'react-native-linear-gradient'
import moment from 'moment'
import styles from './styles'
import * as colors from '../../commons/colors'
import * as dreamStates from '../../commons/dreamStates'

export default class DreamCell extends React.Component {
    static defaultProps = {
        item: {},
        onPress: () => {},
        height: 80
    }

    render() {
        const { item, onPress, height } = this.props
        const source = item.image ? { uri: item.image } : require('../../commons/images/default.png')
        let labelColor = colors.darkGrey
        if (item.state == dreamStates.STATE_CANCELED) {
            labelColor = colors.dreamCanceled
        } else if (item.state == dreamStates.STATE_COMPLETED) {
            labelColor = colors.dreamCompleted
        }
        let favourites = item.favourites ? Object.keys(item.favourites).length : 0
        
        return (
            <TouchableOpacity 
                style={[styles.container, { height: height }]} 
                onPress={onPress}
            >
                <CachedImage source={source} style={styles.image} />
                <View style={styles.content}>
                    <View style={styles.labelView}>
                        <Text style={[styles.label, { color: labelColor }]}>{item.name}</Text>
                        <LinearGradient colors={['rgba(251, 251, 251, 0)', colors.lightWhite]} start={{ x: 0, y: 0.44 }} end={{ x: 0, y: 0.76 }} style={styles.linearGradient} />
                    </View>
                    <View style={styles.favouritesView}>
                        <View style={styles.favourites}>
                            <Icon style={styles.favouritesIcon} name="heart" size={10} solid={favourites > 0} />
                            <Text style={styles.favouritesText}>{favourites}</Text>
                        </View>
                    </View>
                    <View style={styles.metadataView}>
                        <View style={styles.metadata}>
                            <Text style={styles.locationText}>{item.location}</Text>
                            <Text style={styles.bull}>&bull;</Text>
                            <Text style={styles.dateText}>{moment(item.date.toDate()).fromNow()}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}