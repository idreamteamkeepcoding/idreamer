import { StyleSheet } from 'react-native'
import * as colors from '../../commons/colors'

export default StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 28,
        marginLeft: 18
    },
    icon: {
        lineHeight: 28
    },
    number: {
        color: colors.grey,
        fontSize: 18,
        marginLeft: 4
    }
})