import React from 'react'
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import styles from './styles'
import * as colors from '../../commons/colors'

export default class IconNumber extends React.Component {
    static defaultProps = {
        icon: '',
        number: '',
        iconColor: colors.main
    }

    render() {
        const { 
            icon, 
            number,
            iconColor 
        } = this.props
        return (
            <View style={styles.container}>
                <Icon style={styles.icon} name={icon} size={18} color={iconColor} solid={true} />
                <Text style={styles.number}>{number}</Text>
            </View>
        )
    }
}