import firebase from 'react-native-firebase'
import ImageResizer from 'react-native-image-resizer'
import moment from 'moment'
import 'moment/locale/es'

const MAX_IMAGE_SIZE = 1280

// Some app static config
export function configureGlobals() {
    global.categories = [
        {
            id: 0,
            name: 'Cualquiera',
            icon: 'star'
        },
        {
            id: 1,
            name: 'Actividades',
            icon: 'dice-five'
        },
        {
            id: 2,
            name: 'Deportes',
            icon: 'running'
        },
        {
            id: 3,
            name: 'Niños',
            icon: 'child'
        },
        {
            id: 4,
            name: 'Animales',
            icon: 'dog'
        },
        {
            id: 5,
            name: 'Otros',
            icon: 'ellipsis-h'
        }
    ]
    global.ANONYMOUS = 'Pobrecito soñador'
    global.imagePickerOptions = {
        noData: true,
        cancelButtonTitle: 'Cancelar',
        takePhotoButtonTitle: 'Hacer foto',
        chooseFromLibraryButtonTitle: 'Escoger de librería',
        mediaType: 'photo'
    }
    console.disableYellowBox = true
    moment.locale('es')
}

// Uploads the image to the server (currently Firebase). 
// If image dimensions are greater than MAX_IMAGE_SIZE it previously resizes the image.
export function uploadImage(imageUri, width, height, path, successCallback, errorCallback) {
    let resized = false, newWidth = 0, newHeight = 0
    if (width > MAX_IMAGE_SIZE) {
        newWidth = MAX_IMAGE_SIZE
        newHeight = Math.round(height * MAX_IMAGE_SIZE / width)
        resized = true
    } else if (height > MAX_IMAGE_SIZE) {
        newHeight = MAX_IMAGE_SIZE
        newWidth = Math.round(width * MAX_IMAGE_SIZE / height)
        resized = true
    }
    
    if (resized) {
        ImageResizer.createResizedImage(imageUri, newWidth, newHeight, 'JPEG', 75).then((response) => {
            uploadImageToFirebase(response.uri, newWidth, newHeight, path, successCallback, errorCallback)
        }).catch((error) => {
            errorCallback(error)
        })
    } else {
        uploadImageToFirebase(imageUri, width, height, path, successCallback, errorCallback)
    }
}

function uploadImageToFirebase(imageUri, width, height, path, successCallback, errorCallback) {
    firebase.storage().ref(path)
        .putFile(imageUri)
        .on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            snapshot => {
                if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
                    successCallback(snapshot, width, height)
                }
            },
            error => {
                errorCallback(error)
            }
        )
}
