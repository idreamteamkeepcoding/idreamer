export const main = '#0c80d8'
export const darkMain = '#164c84'
export const lightMain = '#99cfef'
export const navBar = main

export const white = '#f2f2f2'
export const darkWhite = '#ddd'
export const lightWhite = '#fbfbfb'

export const black = '#333'
export const grey = '#888'
export const lightGrey = '#ccc'
export const darkGrey = '#444'

export const border = '#999'
export const red = '#d24d57'

export const inputBg = '#85bfeb'

export const messageBorder = '#8dd55d'
export const messageBg = '#b7e6af'
export const message = '#072502'
export const errorBorder = '#dc7777'
export const errorBg = '#ffcece'
export const error = '#9e1414'

export const dreamCanceledBg = '#efcaca'
export const dreamCompletedBg = '#cae8ca'
export const dreamCanceled = '#a00c0c'
export const dreamCompleted = '#0d791f'