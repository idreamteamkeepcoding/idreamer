'use strict';

// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

// Notifications to other members of the chat
exports.newChatMessageNotification = functions.firestore.document('chats/{dreamId}')
    .onWrite(async (change, context) => {

        const dreamId = context.params.dreamId;
        const previousChat = change.before.data();
        const changedChat = change.after.data();

        const previousMessages = previousChat.messages ? previousChat.messages.length : 0;
        const changedMessages = changedChat.messages ? changedChat.messages.length : 0;

        // Chat can also change because it's just created
        if (changedMessages > previousMessages) {
            // Get dream data and last message
            const dreamDoc = await admin.firestore().doc(`dreams/${dreamId}`).get();
            const dream = dreamDoc.data();
            const lastMessage = changedChat.messages[changedChat.messages.length - 1];
            const lastMessageText = lastMessage.message && lastMessage.message !== '' ? lastMessage.message : null;
            const userName = lastMessage.user && lastMessage.user.name ? lastMessage.user.name : 'Usuario anónimo';
            const body = lastMessageText ? `${userName}: ${lastMessageText}` : `${userName} ha publicado una imagen`;

            // Notification payload
            const payload = {
                notification: {
                    title: `Nuevo mensaje en "${dream.name}"`,
                    body
                }
            };

            // Get users to notify and send PUSH notifications
            let users = Object.keys(dream.users).filter(u => u !== lastMessage.user.id);
            if (dream.user !== lastMessage.user.id) {
                users.push(dream.user);
            }
            users.forEach(async (user, index) => {
                const userDoc = await admin.firestore().doc(`users/${user}`).get();
                const userData = userDoc.data();
                if (userData.deviceTokens) {
                    const tokens = Object.keys(userData.deviceTokens);
                    if (tokens.length > 0) {
                        console.log('Sending newChatMessageNotification to', userData.name ? userData.name : 'Usuario anónimo', tokens);
                        admin.messaging().sendToDevice(tokens, payload);
                    }
                }
            });
        }
});

// Notifications on dream changes
exports.changedDreamNotification = functions.firestore.document('dreams/{dreamId}')
    .onWrite(async (change, context) => {
        const updatedDream = change.after.data();
        const previousDream = change.before.data();

        // Check if collaborators have changed
        const previousUsers = previousDream.users ? Object.keys(previousDream.users).length : 0;
        const updatedUsers = updatedDream.users ? Object.keys(updatedDream.users).length : 0;

        if (previousUsers !== updatedUsers) {
            const moreOrLess = previousUsers > updatedUsers ? 'menos' : 'más';
            const payload = {
                notification: {
                    title: `Sueño "${updatedDream.name}"`,
                    body: `Tienes un colaborador ${moreOrLess}`
                }
            }

            // Send PUSH notification to dream's owner
            const dreamOwnerDoc = await admin.firestore().doc(`users/${updatedDream.user}`).get();
            const dreamOwner = dreamOwnerDoc.data();
            if (dreamOwner.deviceTokens) {
                const tokens = Object.keys(dreamOwner.deviceTokens);
                if (tokens.length > 0) {
                    console.log('Sending changedDreamNotification (collaborators) to', dreamOwner.name ? dreamOwner.name : 'Usuario anónimo', tokens);
                    admin.messaging().sendToDevice(tokens, payload);
                }
            } 
        }
});


