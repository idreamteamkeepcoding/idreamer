# iDreamer

Dreaming is free!

This app is based on [React Native](https://facebook.github.io/react-native/) and has versions for iOS and Android. Users can register, share their dreams and collaborate on other users' dreams.

## Usage

You need a recent [node.js](https://nodejs.org/) installation to make it work. 

Clone or download the repository and install dependencies executing:

```shell
npm i
```

After that, you have to start the development server with:

```shell
npm start
```

After [installing React Native](https://facebook.github.io/react-native/docs/getting-started) on your system, the app can be started with:

```shell
react-native run-ios
```

Or:

```shell
react-native run-android
```

## Installing the app in a actual device

The app can be installed in a phone or a tablet, and the instructions to make that possible depend on your OS and the type of device. You can refer to the [full instructions in the React Native documentation](https://facebook.github.io/react-native/docs/running-on-device).

## Screenshots

![iDreamer Launch](screenshots/screenshot1.png)
![iDreamer Login](screenshots/screenshot2.png)
![iDreamer Main](screenshots/screenshot3.png)
![iDreamer Detail](screenshots/screenshot4.png)
![iDreamer Share](screenshots/screenshot5.png)
![iDreamer Searches](screenshots/screenshot6.png)
![iDreamer Chat](screenshots/screenshot7.png)
![iDreamer User](screenshots/screenshot8.png)